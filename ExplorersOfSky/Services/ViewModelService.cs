using System;
using System.Reactive.Disposables;
using System.Windows.Forms;
using ExplorersOfSky.UI.ViewModels;
using ExplorersOfSky.UI.ViewModels.Extensible;
using ExplorersOfSky.UI.Views;
using ReactiveUI;
using Splat;

namespace ExplorersOfSky.Services
{
    public interface IViewModelService
    {
        IViewFor GetViewFor<T>(T viewModel) where T : class;
        Form GetViewAsFormFor<T>(T viewModel) where T : class;
    }

    public sealed class ViewModelService : IViewModelService
    {
        private ViewModelService()
        {
            var resolver = Locator.CurrentMutable;

            this.Register<Main, MainViewModel>(resolver);
            this.Register<ARCode, ARCodeViewModel>(resolver);
        }

        public IViewFor GetViewFor<T>(T viewModel) where T : class
        {
            var view = ViewLocator.Current.ResolveView(viewModel);
            view.ViewModel = viewModel;

            if (view.ViewModel is IClipboardServiceInjectable clipboardVM) {
                clipboardVM.ClipboardService = Locator.Current.GetService<IClipboardService>();
            }

            return view;
        }

        public Form GetViewAsFormFor<T>(T viewModel) where T : class => (Form)this.GetViewFor(viewModel);

        private void Register<T, TViewModel>(IMutableDependencyResolver resolver)
            where T : IViewFor<TViewModel>, new()
            where TViewModel : class
            => resolver.Register(() => new T(), typeof(IViewFor<TViewModel>));

        public static IDisposable Initialize()
        {
            Locator.CurrentMutable.RegisterLazySingleton<IViewModelService>(() => new ViewModelService());

            return Disposable.Empty;
        }
    }
}
