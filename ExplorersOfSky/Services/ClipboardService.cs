using System;
using System.Reactive.Disposables;
using System.Windows.Forms;
using Splat;

namespace ExplorersOfSky.Services
{
    public interface IClipboardService
    {
        void SetData<T>(T data);
    }

    public sealed class ClipboardService : IClipboardService
    {
        public void SetData<T>(T data)
            => Clipboard.SetDataObject(data);

        public static IDisposable Initialize()
        {
            Locator.CurrentMutable.RegisterLazySingleton<IClipboardService>(() => new ClipboardService());

            return Disposable.Empty;
        }
    }
}
