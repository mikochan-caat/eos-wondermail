using System;
using System.Windows.Forms;
using ExplorersOfSky.Services;
using ExplorersOfSky.UI.ViewModels;
using ReactiveUI;
using Splat;

namespace ExplorersOfSky
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Locator.CurrentMutable.InitializeSplat();
            Locator.CurrentMutable.InitializeReactiveUI();

            using (ClipboardService.Initialize())
            using (ViewModelService.Initialize()) {
                var mainForm = Locator.Current
                    .GetService<IViewModelService>()
                    .GetViewAsFormFor(new MainViewModel());
                Application.Run(mainForm);
            }
        }
    }
}
