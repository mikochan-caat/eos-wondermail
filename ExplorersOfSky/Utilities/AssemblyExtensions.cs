using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ExplorersOfSky.Utilities
{
    public static class AssemblyExtensions
    {
        public static AppInfo GetAppInfo(this Assembly assembly) => new AppInfo {
            ProductName = assembly.GetCustomAttribute<AssemblyProductAttribute>()?.Product ?? String.Empty,
            Guid = assembly.GetCustomAttribute<GuidAttribute>()?.Value ?? Guid.Empty.ToString(),
            FileVersion = assembly.GetCustomAttribute<AssemblyFileVersionAttribute>()?.Version ?? String.Empty,
            Build = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion ?? String.Empty,
            Description = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description ?? String.Empty,
            Copyright = assembly.GetCustomAttribute<AssemblyCopyrightAttribute>()?.Copyright ?? String.Empty
        };

        public sealed class AppInfo
        {
            public string ProductName { get; set; }
            public string Guid { get; set; }
            public string FileVersion { get; set; }
            public string Build { get; set; }
            public string Description { get; set; }
            public string Copyright { get; set; }
        }
    }
}
