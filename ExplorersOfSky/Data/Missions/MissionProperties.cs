using System.Collections.Generic;

namespace ExplorersOfSky.Data.Missions
{
    public interface IMissionProperties
    {
        SpecialType? SpecialType { get; }

        bool IsClientTarget { get; }
        bool UseTargetItem { get; }

        Pokemon ForcedClient { get; }
        Pokemon ForcedTarget { get; }

        bool HasNoReward { get; }

        IReadOnlyList<byte> SpecialFloorLayouts { get; }

        public bool HasForcedClient { get; }
        public bool HasForcedTarget { get; }
    }

    public sealed class MissionProperties : IMissionProperties
    {
        public SpecialType? SpecialType { get; set; }
        public bool IsClientTarget { get; set; }
        public bool UseTargetItem { get; set; }
        public Pokemon ForcedClient { get; set; }
        public Pokemon ForcedTarget { get; set; }
        public bool HasNoReward { get; set; }
        public IReadOnlyList<byte> SpecialFloorLayouts { get; set; }

        public bool HasForcedClient => this.ForcedClient != Pokemon.None;
        public bool HasForcedTarget => this.ForcedTarget != Pokemon.None;

        public MissionProperties()
        {
            this.SpecialFloorLayouts = new List<byte>();
            this.ForcedClient = Pokemon.None;
            this.ForcedTarget = Pokemon.None;
        }
    }
}
