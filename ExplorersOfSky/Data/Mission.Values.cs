using System.Collections.Generic;
using ExplorersOfSky.Data.Missions;

namespace ExplorersOfSky.Data
{
    public sealed partial class Mission
    {
        public static Mission RescueClient = new Mission("Rescue Client", 0) {
            MissionType = MissionType.Type0,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0,
                IsClientTarget = true
            }
        };
        public static Mission RescueTarget = new Mission("Rescue Target", 1) {
            MissionType = MissionType.Type1,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0
            }
        };
        public static Mission EscortToTarget = new Mission("Escort to Target", 2) {
            MissionType = MissionType.Type2,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0
            }
        };
        public static Mission ExploreWithClient = new Mission("Explore with Client", 3) {
            MissionType = MissionType.Type3,
            Properties = new MissionProperties {
                IsClientTarget = true
            },
            SubTypes = new List<ISubType> {
                new SubType {
                    Name = "Normal",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type0
                    }
                },
                new SubType {
                    Name = "Sealed Chamber",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type1,
                        SpecialFloorLayouts = new List<byte> { 165 }
                    }
                },
                new SubType {
                    Name = "Golden Chamber",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type2,
                        SpecialFloorLayouts = new List<byte> { 111 }
                    }
                }
            }
        };
        public static Mission ProspectWithClient = new Mission("Prospect with Client", 4) {
            MissionType = MissionType.Type4,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0,
                UseTargetItem = true,
                IsClientTarget = true
            }
        };
        public static Mission GuideClient = new Mission("Guide Client", 5) {
            MissionType = MissionType.Type5,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0,
                IsClientTarget = true
            }
        };
        public static Mission FindTargetItem = new Mission("Find Target Item", 6) {
            MissionType = MissionType.Type6,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0,
                UseTargetItem = true,
                IsClientTarget = true
            }
        };
        public static Mission DeliverTargetItem = new Mission("Deliver Target Item", 7) {
            MissionType = MissionType.Type7,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0,
                UseTargetItem = true,
                IsClientTarget = true
            }
        };
        public static Mission SearchClient = new Mission("Search for Client", 8) {
            MissionType = MissionType.Type8,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0
            }
        };
        public static Mission StealFromTarget = new Mission("Steal from Target", 9) {
            MissionType = MissionType.Type9,
            Properties = new MissionProperties {
                UseTargetItem = true
            },
            SubTypes = new List<ISubType> {
                new SubType {
                    Name = "Normal",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type0
                    }
                },
                new SubType {
                    Name = "Target is hidden",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type1
                    }
                },
                new SubType {
                    Name = "Target runs away",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type2
                    }
                }
            }
        };
        public static Mission ArrestMagnemite = new Mission("Arrest Target (Client: Magnemite)", 10) {
            MissionType = MissionType.Type10,
            Properties = new MissionProperties {
                ForcedClient = Pokemon.Magnemite
            },
            SubTypes = new List<ISubType> {
                new SubType {
                    Name = "Normal",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type0
                    }
                },
                new SubType {
                    Name = "Escort",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type4
                    }
                },
                new SubType {
                    Name = "Monster House",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type7
                    }
                }
            }
        };
        public static Mission ArrestMagnezone = new Mission("Arrest Target (Client: Magnezone)", 11) {
            MissionType = MissionType.Type10,
            Properties = new MissionProperties {
                ForcedClient = Pokemon.Magnezone
            },
            SubTypes = new List<ISubType> {
                new SubType {
                    Name = "Normal",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type0
                    }
                },
                new SubType {
                    Name = "Escort",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type4
                    }
                },
                new SubType {
                    Name = "Monster House",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type7
                    }
                }
            }
        };
        public static Mission ChallengeRequest = new Mission("Challenge Request", 12) {
            MissionType = MissionType.Type11,
            Properties = new MissionProperties {},
            SubTypes = new List<ISubType> {
                new SubType {
                    Name = "Mewtwo",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type1,
                        ForcedClient = Pokemon.Mewtwo,
                        ForcedTarget = Pokemon.Mewtwo,
                        SpecialFloorLayouts = new List<byte> { 145 }
                    }
                },
                new SubType {
                    Name = "Entei",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type2,
                        ForcedClient = Pokemon.Entei,
                        ForcedTarget = Pokemon.Entei,
                        SpecialFloorLayouts = new List<byte> { 146 }
                    }
                },
                new SubType {
                    Name = "Raikou",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type3,
                        ForcedClient = Pokemon.Raikou,
                        ForcedTarget = Pokemon.Raikou,
                        SpecialFloorLayouts = new List<byte> { 147 }
                    }
                },
                new SubType {
                    Name = "Suicune",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type4,
                        ForcedClient = Pokemon.Suicune,
                        ForcedTarget = Pokemon.Suicune,
                        SpecialFloorLayouts = new List<byte> { 148 }
                    }
                },
                new SubType {
                    Name = "Jirachi",
                    Properties = new MissionProperties {
                        SpecialType = SpecialType.Type5,
                        ForcedClient = Pokemon.Jirachi,
                        ForcedTarget = Pokemon.Jirachi,
                        SpecialFloorLayouts = new List<byte> { 149 }
                    }
                }
            }
        };
        public static Mission TreasureHunt = new Mission("Treasure Hunt", 13) {
            MissionType = MissionType.Type12,
            Properties = new MissionProperties {
                SpecialType = SpecialType.Type0,
                ForcedClient = Pokemon.Turtwig,
                ForcedTarget = Pokemon.Turtwig,
                HasNoReward = true,
                SpecialFloorLayouts = new List<byte> {
                    114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
                    130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144
                }
            }
        };
    }
}
