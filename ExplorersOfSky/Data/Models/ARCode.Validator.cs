using FluentValidation;

namespace ExplorersOfSky.Data.Models
{
    public sealed partial class ARCode
    {
        private sealed class Validator : AbstractValidator<ARCode>
        {
            public Validator()
            {
                this.RuleFor(model => model.WonderMailCode)
                    .NotNull()
                    .NotEmpty()
                    .WithName("Wonder Mail code")
                    .WithRequiredMessage();
            }
        }
    }
}
