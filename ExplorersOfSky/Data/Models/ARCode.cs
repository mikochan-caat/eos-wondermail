using System;
using System.Linq;
using System.Text;
using FluentValidation;
using FluentValidation.Results;

namespace ExplorersOfSky.Data.Models
{
    public sealed partial class ARCode
    {
        private const uint InitialCodeOffset = 0x021ED4B8;
        private const uint ReenterCodeOffset = 0x021EF760;
        private const uint MemoryAddressCheckOffset = 0x4;
        private const int WonderMailARLineLength = 8;
        private const int WonderMailSourceLength = WonderMailARLineLength / 2;
        private const char PaddingChar = '0';

        public string WonderMailCode { get; set; }

        public string Generate()
        {
            new Validator().ValidateAndThrow(this);

            return String.Join(Environment.NewLine, new[] {
                this.GenerateCode(InitialCodeOffset),
                String.Empty,
                this.GenerateCode(ReenterCodeOffset)
            });
        }

        public ValidationResult Validate()
            => new Validator().Validate(this);

        private string GenerateCode(uint codeOffset)
        {
            string checkOffsetStr = OffsetToHex(codeOffset - MemoryAddressCheckOffset)[1..];
            string codeOffsetStr = OffsetToHex(codeOffset);

            var builder = new StringBuilder();
            builder.AppendLine("94000130 000001F7");
            builder.AppendLine($"5{checkOffsetStr} {codeOffsetStr}");

            for (int i = 0; i < this.WonderMailCode.Length; i += WonderMailSourceLength)
            {
                string offset = OffsetToHex(codeOffset + (uint)i);
                int endLength = this.WonderMailCode.Length - i;
                var arBytes = Encoding.ASCII
                    .GetBytes(this.WonderMailCode.Substring(i, Math.Min(WonderMailSourceLength, endLength)))
                    .Reverse()
                    .Select(wmByte => OffsetToHex(wmByte, 2));
                string arValue = String
                    .Join(String.Empty, arBytes)
                    .PadLeft(WonderMailARLineLength, PaddingChar);

                builder.AppendLine($"{offset} {arValue}");
            }

            builder.AppendLine("D0000000 00000000");
            builder.AppendLine("D0000000 00000000");
            return builder.ToString().Trim();
        }

        private static string OffsetToHex(uint offset, int length = WonderMailARLineLength)
            => Convert.ToString(offset, 16).ToUpperInvariant().PadLeft(length, PaddingChar);
    }
}
