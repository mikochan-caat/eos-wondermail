using System;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace ExplorersOfSky.Data.Models
{
    public static class ValidationExtensions
    {
        public static string Summarize(this ValidationResult result, string summaryMessage, string endMessage = null)
        {
            var summaryBody = result.Errors.Select(err => $"    > {err.ErrorMessage}");
            var summary = (new[] { summaryMessage })
                .Concat(summaryBody)
                .Concat(new[] { $"{Environment.NewLine}{endMessage}" })
                .Where(value => !String.IsNullOrWhiteSpace(value));

            return String.Join(Environment.NewLine, summary);
        }
    }
}
