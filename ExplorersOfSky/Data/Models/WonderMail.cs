using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using FluentValidation.Results;

namespace ExplorersOfSky.Data.Models
{
    public sealed partial class WonderMail
    {
        private static readonly IReadOnlyList<uint> ChecksumLookup = CreateChecksumLookup();
        private static readonly IReadOnlyList<uint> EncryptionLookup = CreateEncryptionLookup();
        private static readonly IReadOnlyList<int> DefaultByteSwap = CreateDefaultByteSwap();
        private static readonly IReadOnlyList<int> EUByteSwap = CreateEUByteSwap();
        private static readonly IReadOnlyList<WMStructInfo> WMStruct = CreateWMStruct();

        public uint NullBits => 0;
        public uint SpecialFloor { get; set; }
        public uint FloorNumber { get; set; }
        public uint Dungeon { get; set; }
        public uint FlavorText { get; }
        public uint Restriction => 0;
        public uint RestrictionType => 0;
        public uint RewardItem { get; set; }
        public uint RewardType { get; set; }
        public uint TargetItem { get; set; }
        public uint ExtraTarget => 0;
        public uint Target { get; set; }
        public uint Client { get; set; }
        public uint MissionSubType { get; set; }
        public uint Mission { get; set; }
        public uint MailType => 0x4;

        public bool UseEUByteSwap { get; set; }
        public bool ValidateClient { get; set; }
        public bool ValidateRewardItem { get; set; }
        public bool HasNoReward { get; set; }

        public WonderMail()
        {
            this.FlavorText = (uint)new Random().Next(300000, 400001);
        }

        public string Generate()
        {
            new Validator().ValidateAndThrow(this);

            var builder = new StringBuilder();
            for (int i = 0; i < WMStruct.Count; i++) {
                var info = WMStruct[i];
                builder.Append(IntegerToBits(info.GetFieldValue(this), info.Size));
            }

            string bitsData = builder.ToString(2, builder.Length - 2);
            uint checksum = CalculateChecksum(bitsData);
            string checksumBits = IntegerToBits(checksum, 32);
            string wonderMailBits = $"00{bitsData}{checksumBits}";
            string encryptedData = this.EncryptWonderMail(wonderMailBits);
            string encodedData = this.EncodeWonderMail(encryptedData);

            return this.FinalizeWonderMail(encodedData);
        }

        public ValidationResult Validate()
            => new Validator().Validate(this);

        // ---------------------------------------------------------------------
        //  Process backbone starts here
        //  Sourced from: https://dengler9.github.io/wmsgenerator/
        //
        //  Some parts modified to reduce redundant and unnecessary portions.
        // ---------------------------------------------------------------------
        private string FinalizeWonderMail(string encodedWonderMail)
        {
            var swapArray = this.UseEUByteSwap ? EUByteSwap : DefaultByteSwap;
            var output = new List<string>();

            for (int i = 0; i < swapArray.Count; i++) {
                output.Add(String.Empty);
            }
            for (int i = 0; i < swapArray.Count; i++) {
                int target = swapArray[i];
                output[target] += encodedWonderMail[i];
            }

            return String.Join(String.Empty, output);
        }

        private string EncryptWonderMail(string wonderMailBits)
        {
            var blocks = new List<uint>();

            int bitPtr = wonderMailBits.Length - 8;
            string checksumBits = wonderMailBits.Substring(bitPtr, 8);
            uint checksumByte = BitsToInteger(checksumBits);

            bitPtr -= 24;
            string skyChecksumBits = wonderMailBits.Substring(bitPtr, 24);
            uint fullChecksum = BitsToInteger(skyChecksumBits + checksumBits);

            while (bitPtr > 7) {
                bitPtr -= 8;
                uint data = BitsToInteger(wonderMailBits.Substring(bitPtr, 8));
                blocks.Add(data);
            }

            var encryptionEntries = GetEncryptionEntries(checksumByte);
            uint? resetByte = GetResetByte(fullChecksum);

            int tblPtr = 0;
            int encPtr = 0;
            for (int i = 0; i < blocks.Count; i++) {
                if (encPtr == resetByte) {
                    encPtr = 0;
                }

                uint inputByte = blocks[tblPtr];
                uint result = (inputByte + encryptionEntries[encPtr]) & 0xFF;
                blocks[i] = result;

                tblPtr++;
                encPtr++;
            }

            string twoBitsStart = wonderMailBits.Substring(0, 2);
            var builder = new StringBuilder(twoBitsStart);

            for (int blockPtr = blocks.Count - 1; blockPtr >= 0; blockPtr--) {
                builder.Append(IntegerToBits(blocks[blockPtr], 8));
            }
            builder.Append(skyChecksumBits);
            builder.Append(checksumBits);

            return builder.ToString();
        }

        private string EncodeWonderMail(string encryptedWonderMail)
        {
            int blocks = encryptedWonderMail.Length / 5;
            string mailChars = "&67NPR89F0+#STXY45MCHJ-K12=%3Q@W";

            var builder = new StringBuilder();
            for (int i = 0; i < blocks; i++) {
                string currChars = encryptedWonderMail.Substring((blocks - i - 1) * 5, 5);
                uint num = BitsToInteger(currChars);
                if (num >= 0 && num < 32) {
                    builder.Append(mailChars[(int)num]);
                } else {
                    throw new InvalidOperationException($"Could not find '{currChars}' in the Wonder Mail " +
                                                         "character lookup. This is a bug!");
                }
            }

            return builder.ToString();
        }

        private static uint CalculateChecksum(string bits)
        {
            uint checksum = 0xFFFFFFFF;

            for (int i = 16; i >= 0; i--) {
                string subBits = bits.Substring(i * 8, 8);
                uint numBits = BitsToInteger(subBits);

                uint entry = ChecksumLookup[(int)((checksum ^ numBits) & 0xFF)];
                checksum = (checksum >> 8) ^ entry;
            }

            checksum ^= 0xFFFFFFFF;
            return checksum;
        }

        private static uint? GetResetByte(uint checksum)
        {
            uint checksumByte = checksum % 256;
            uint resetByte = (uint)Math.Floor((double)(checksumByte / 16) + 8 + (checksumByte % 16));
            return (resetByte < 17) ? (uint?)resetByte : null;
        }

        private static IReadOnlyList<uint> GetEncryptionEntries(uint checksumByte)
        {
            var entries = new List<uint>();
            int encPointer = (int)checksumByte;
            bool backwards = !Convert.ToBoolean(checksumByte & 0x1);

            for (int i = 0; i < 17; i++) {
                entries.Add(EncryptionLookup[encPointer]);
                if (backwards) {
                    encPointer--;
                    if (encPointer < 0) {
                        encPointer = EncryptionLookup.Count - 1;
                    }
                } else {
                    encPointer++;
                    if (encPointer >= EncryptionLookup.Count) {
                        encPointer = 0;
                    }
                }
            }

            return entries;
        }

        private static IReadOnlyList<uint> CreateChecksumLookup()
        {
            uint[] lookup = new uint[256];
            for (uint i = 0; i < lookup.Length; i++) {
                uint entry = i;

                for (int j = 0; j < 8; j++) {
                    if (!Convert.ToBoolean(entry & 1)) {
                        entry >>= 1;
                    } else {
                        entry = 0xEDB88320 ^ (entry >> 1);
                    }

                    lookup[i] = entry;
                }
            }

            return lookup;
        }

        private static IReadOnlyList<uint> CreateEncryptionLookup()
            => new uint[] {
                // Listed vertical: first part of the 2-character hex code range
                // Listed horizontal: second part of the 2-character hex code
                // 0     1     2     3     4     5     6     7     8     9     A     B     C     D     E     F
                0x2E, 0x75, 0x3F, 0x99, 0x09, 0x6C, 0xBC, 0x61, 0x7C, 0x2A, 0x96, 0x4A, 0xF4, 0x6D, 0x29, 0xFA, // 00-0F
                0x90, 0x14, 0x9D, 0x33, 0x6F, 0xCB, 0x49, 0x3C, 0x48, 0x80, 0x7B, 0x46, 0x67, 0x01, 0x17, 0x59, // 10-1F
                0xB8, 0xFA, 0x70, 0xC0, 0x44, 0x78, 0x48, 0xFB, 0x26, 0x80, 0x81, 0xFC, 0xFD, 0x61, 0x70, 0xC7, // 20-2F
                0xFE, 0xA8, 0x70, 0x28, 0x6C, 0x9C, 0x07, 0xA4, 0xCB, 0x3F, 0x70, 0xA3, 0x8C, 0xD6, 0xFF, 0xB0, // 30-3F
                0x7A, 0x3A, 0x35, 0x54, 0xE9, 0x9A, 0x3B, 0x61, 0x16, 0x41, 0xE9, 0xA3, 0x90, 0xA3, 0xE9, 0xEE, // 40-4F
                0x0E, 0xFA, 0xDC, 0x9B, 0xD6, 0xFB, 0x24, 0xB5, 0x41, 0x9A, 0x20, 0xBA, 0xB3, 0x51, 0x7A, 0x36, // 50-5F
                0x3E, 0x60, 0x0E, 0x3D, 0x02, 0xB0, 0x34, 0x57, 0x69, 0x81, 0xEB, 0x67, 0xF3, 0xEB, 0x8C, 0x47, // 60-6F
                0x93, 0xCE, 0x2A, 0xAF, 0x35, 0xF4, 0x74, 0x87, 0x50, 0x2C, 0x39, 0x68, 0xBB, 0x47, 0x1A, 0x02, // 70-7F
                0xA3, 0x93, 0x64, 0x2E, 0x8C, 0xAD, 0xB1, 0xC4, 0x61, 0x04, 0x5F, 0xBD, 0x59, 0x21, 0x1C, 0xE7, // 80-8F
                0x0E, 0x29, 0x26, 0x97, 0x70, 0xA9, 0xCD, 0x18, 0xA3, 0x7B, 0x74, 0x70, 0x96, 0xDE, 0xA6, 0x72, // 90-9F
                0xDD, 0x13, 0x93, 0xAA, 0x90, 0x6C, 0xA7, 0xB5, 0x76, 0x2F, 0xA8, 0x7A, 0xC8, 0x81, 0x06, 0xBB, // A0-AF
                0x85, 0x75, 0x11, 0x0C, 0xD2, 0xD1, 0xC9, 0xF8, 0x81, 0x70, 0xEE, 0xC8, 0x71, 0x53, 0x3D, 0xAF, // B0-BF
                0x76, 0xCB, 0x0D, 0xC1, 0x56, 0x28, 0xE8, 0x3C, 0x61, 0x64, 0x4B, 0xB8, 0xEF, 0x3B, 0x41, 0x09, // C0-CF
                0x72, 0x07, 0x50, 0xAD, 0xF3, 0x2E, 0x5C, 0x43, 0xFF, 0xC3, 0xB3, 0x32, 0x7A, 0x3E, 0x9C, 0xA3, // D0-DF
                0xC2, 0xAB, 0x10, 0x60, 0x99, 0xFB, 0x08, 0x8A, 0x90, 0x57, 0x8A, 0x7F, 0x61, 0x90, 0x21, 0x88, // E0-EF
                0x55, 0xE8, 0xFC, 0x4B, 0x0D, 0x4A, 0x7A, 0x48, 0xC9, 0xB0, 0xC7, 0xA6, 0xD0, 0x04, 0x7E, 0x05  // F0-FF
            };

        private static IReadOnlyList<int> CreateDefaultByteSwap()
            => new int[] {
                0x07, 0x1B, 0x0D, 0x1F, 0x15, 0x1A, 0x06, 0x01,
                0x17, 0x1C, 0x09, 0x1E, 0x0A, 0x20, 0x10, 0x21,
                0x0F, 0x08, 0x1D, 0x11, 0x14, 0x00, 0x13, 0x16,
                0x05, 0x12, 0x0E, 0x04, 0x03, 0x18, 0x02, 0x0B,
                0x0C, 0x19
            };

        private static IReadOnlyList<int> CreateEUByteSwap()
            => new int[] {
                0x0E, 0x04, 0x03, 0x18, 0x09, 0x1E, 0x0A, 0x20,
                0x10, 0x21, 0x14, 0x00, 0x13, 0x16, 0x05, 0x12,
                0x06, 0x01, 0x17, 0x1C, 0x07, 0x1B, 0x0D, 0x1F,
                0x15, 0x1A, 0x02, 0x0B, 0x0C, 0x19, 0x0F, 0x08,
                0x1D, 0x11
            };

        private static uint BitsToInteger(string value)
            => Convert.ToUInt32(value, 2);

        private static string IntegerToBits(uint value, int outputSize)
        {
            string binary = Convert.ToString(value, 2).PadLeft(outputSize, '0');
            return binary.Substring(Math.Max(0, binary.Length - outputSize), outputSize);
        }

        private static List<WMStructInfo> CreateWMStruct()
            => new List<WMStructInfo> {
            new WMStructInfo {
                FieldName = nameof(NullBits), Size = 8,
                GetFieldValue = (mail) => mail.NullBits
            },
            new WMStructInfo {
                FieldName = nameof(SpecialFloor), Size = 8,
                GetFieldValue = (mail) => mail.SpecialFloor
            },
            new WMStructInfo {
                FieldName = nameof(FloorNumber), Size = 8,
                GetFieldValue = (mail) => mail.FloorNumber
            },
            new WMStructInfo {
                FieldName = nameof(Dungeon), Size = 8,
                GetFieldValue = (mail) => mail.Dungeon
            },
            new WMStructInfo {
                FieldName = nameof(FlavorText), Size = 24,
                GetFieldValue = (mail) => mail.FlavorText
            },
            new WMStructInfo {
                FieldName = nameof(Restriction), Size = 11,
                GetFieldValue = (mail) => mail.Restriction
            },
            new WMStructInfo {
                FieldName = nameof(RestrictionType), Size = 1,
                GetFieldValue = (mail) => mail.RestrictionType
            },
            new WMStructInfo {
                FieldName = nameof(RewardItem), Size = 11,
                GetFieldValue = (mail) => mail.RewardItem
            },
            new WMStructInfo {
                FieldName = nameof(RewardType), Size = 4,
                GetFieldValue = (mail) => mail.RewardType
            },
            new WMStructInfo {
                FieldName = nameof(TargetItem), Size = 10,
                GetFieldValue = (mail) => mail.TargetItem
            },
            new WMStructInfo {
                FieldName = nameof(ExtraTarget), Size = 11,
                GetFieldValue = (mail) => mail.ExtraTarget
            },
            new WMStructInfo {
                FieldName = nameof(Target), Size = 11,
                GetFieldValue = (mail) => mail.Target
            },
            new WMStructInfo {
                FieldName = nameof(Client), Size = 11,
                GetFieldValue = (mail) => mail.Client
            },
            new WMStructInfo {
                FieldName = nameof(MissionSubType), Size = 4,
                GetFieldValue = (mail) => mail.MissionSubType
            },
            new WMStructInfo {
                FieldName = nameof(Mission), Size = 4,
                GetFieldValue = (mail) => mail.Mission
            },
            new WMStructInfo {
                FieldName = nameof(MailType), Size = 4,
                GetFieldValue = (mail) => mail.MailType
            }
        };

        private sealed class WMStructInfo
        {
            public string FieldName { get; set; }
            public int Size { get; set; }
            public Func<WonderMail, uint> GetFieldValue { get; set; }
        }
    }
}
