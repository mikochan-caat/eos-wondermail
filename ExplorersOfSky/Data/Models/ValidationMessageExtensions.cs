using System;
using FluentValidation;

namespace ExplorersOfSky.Data.Models
{
    public static class ValidationMessageExtensions
    {
        public static IRuleBuilderOptions<TModel, TValue> WithRequiredMessage<TModel, TValue>(
            this IRuleBuilderOptions<TModel, TValue> builder)
            => builder.WithMessage("{PropertyName} is required.");

        public static IRuleBuilderOptions<TModel, TValue> WithInvalidSelectionMessage<TModel, TValue>(
            this IRuleBuilderOptions<TModel, TValue> builder, Func<TValue, string> valueSelector, string identifier = null)
            => builder.WithMessage((_, value)
                => $"{valueSelector(value)} is not a valid {identifier ?? "selection"} for {{PropertyName}}.");
    }
}
