using System.Linq;
using FluentValidation;

namespace ExplorersOfSky.Data.Models
{
    public sealed partial class WonderMail
    {
        private sealed class Validator : AbstractValidator<WonderMail>
        {
            public Validator()
            {
                this.RuleFor(model => model.Dungeon)
                    .GreaterThanOrEqualTo(Data.Dungeon.List.OrderBy(d => d.Value).First())
                    .WithName("Dungeon")
                    .WithRequiredMessage();

                this.RuleFor(model => model.FloorNumber)
                    .InclusiveBetween((uint)Constants.MinimumFloorNumber, (uint)Constants.MaximumFloorNumber)
                    .WithName("Floor Number");

                this.Transform(model => model.Client, client => Pokemon.FromGenderSpecificValue(client))
                    .Must(client => client.IsValidClient)
                    .When(model => model.ValidateClient)
                    .WithInvalidSelectionMessage(client => client.Name);

                this.Transform(model => model.TargetItem, item => Item.FromValueEx(item))
                    .Must(item => item.IsValidTarget)
                    .When(model => model.TargetItem != Item.None)
                    .WithInvalidSelectionMessage(item => item.Name);

                this.Transform(model => model.RewardItem, item => Item.FromValueEx(item))
                    .Must(item => item.IsValidReward)
                    .When(model => model.ValidateRewardItem && !model.HasNoReward)
                    .WithInvalidSelectionMessage(item => item.Name);
            }
        }
    }
}
