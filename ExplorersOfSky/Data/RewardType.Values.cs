namespace ExplorersOfSky.Data
{
    public sealed partial class RewardType
    {
        public static readonly RewardType None = new RewardType("None", 255);
        public static readonly RewardType Cash = new RewardType("Cash", 0);
        public static readonly RewardType CashWithItem = new RewardType("Cash + Item", 1);
        public static readonly RewardType Item = new RewardType("Item", 2);
        public static readonly RewardType ItemWithRandom = new RewardType("Item + Random Reward", 3);
        public static readonly RewardType RewardItem = new RewardType("Reward Item", 4);
        public static readonly RewardType Egg = new RewardType("Egg", 5);
        public static readonly RewardType ClientJoins = new RewardType("Client Joins", 6);
    }
}
