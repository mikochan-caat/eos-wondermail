using System;
using System.Collections.Generic;
using System.Linq;
using ExplorersOfSky.Data.Enums;

namespace ExplorersOfSky.Data
{
    public sealed partial class Pokemon : UInt32EnumBase<Pokemon>, INameValuePair
    {
        private const uint FemaleIdBase = 600;

        private static readonly Lazy<HashSet<Pokemon>> FemaleOnly = new Lazy<HashSet<Pokemon>>(() => new HashSet<Pokemon> {
            Happiny, Chansey, Blissey, Cresselia, Froslass, Illumise, Jynx, Kangaskhan, Latias,
            Miltank, Smoochum, Vespiquen, WormadamBlue, WormadamGreen, WormadamPink,
        });
        private static readonly Lazy<HashSet<Pokemon>> GenderLocked =
            new Lazy<HashSet<Pokemon>>(() => new HashSet<Pokemon>(FemaleOnly.Value.Concat(new[] {
                // Nido family
                NidoranF, NidoranM, Nidorino, Nidorina, Nidoking, Nidoqueen,

                // Male only
                Gallade, Hitmonchan, Hitmonlee, Hitmontop, Tyrogue, Latios, Mothim, Tauros, Volbeat,

                // Genderless
                Magnemite, Magneton, Magnezone, Voltorb, Electrode, Staryu, Starmie, Porygon, Porygon2, PorygonZ,
                Shedinja, Lunatone, Solrock, Baltoy, Claydol, Beldum, Metang, Metagross, Bronzor, Bronzong,
                Rotom, Phione, Manaphy, Ditto, Articuno, Zapdos, Moltres, Mewtwo, Mew, UnownA, UnownB, UnownC,
                UnownD, UnownE, UnownF, UnownG, UnownH, UnownI, UnownJ, UnownK, UnownL, UnownM, UnownN, UnownO,
                UnownP, UnownQ, UnownR, UnownS, UnownT, UnownU, UnownV, UnownW, UnownX, UnownY, UnownZ, UnownExclamation,
                UnownQuestion, Raikou, Entei, Suicune, Lugia, HoOh, Celebi, CelebiShiny, Regirock, Regice, Registeel,
                Kyogre, Groudon, Rayquaza, Jirachi, DeoxysA, DeoxysB, DeoxysC, DeoxysD, Azelf, Uxie, Mesprit,
                Dialga, Palkia, Regigigas, Darkrai, Giratina, Shaymin
            })));
        private static readonly Lazy<HashSet<Pokemon>> ValidClients = new Lazy<HashSet<Pokemon>>(() => new HashSet<Pokemon> {
            Bulbasaur, Ivysaur, Venusaur, Charmander, Charmeleon, Charizard, Squirtle, Wartortle, Blastoise, Caterpie,
            Metapod, Butterfree, Weedle, Beedrill, Pidgey, Pidgeot, Rattata, Raticate, Spearow, Fearow, Ekans, Arbok,
            Raichu, Sandshrew, Sandslash, NidoranF, Nidorina, NidoranM, Nidorino, Nidoking, Clefairy, Clefable, Vulpix,
            Ninetales, Jigglypuff, Zubat, Golbat, Oddish, Gloom, Vileplume, Paras, Venonat, Venomoth, Meowth, Persian,
            Psyduck, Golduck, Mankey, Primeape, Growlithe, Arcanine, Poliwag, Poliwhirl, Poliwrath, Kadabra, Alakazam,
            Machop, Machamp, Bellsprout, Weepinbell, Tentacool, Tentacruel, Geodude, Graveler, Golem, Ponyta, Rapidash,
            Slowpoke, Slowbro, Farfetchd, Doduo, Dodrio, Seel, Dewgong, Grimer, Muk, Shellder, Cloyster, Gastly, Haunter,
            Gengar, Onix, Hypno, Krabby, Kingler, Voltorb, Electrode, Exeggcute, Exeggutor, Cubone, Marowak, Hitmonlee,
            Hitmonchan, Lickitung, Koffing, Weezing, Rhyhorn, Rhydon, Tangela, Kangaskhan, Horsea, Seadra, Goldeen, Seaking,
            Staryu, Starmie, Scyther, Jynx, Electabuzz, Magmar, Pinsir, Tauros, Magikarp, Ditto, Eevee, Vaporeon, Jolteon,
            Flareon, Omanyte, Omastar, Kabuto, Kabutops, Aerodactyl, Snorlax, Dratini, Dragonair, Dragonite, Chikorita, Bayleef,
            Meganium, Cyndaquil, Quilava, Totodile, Croconaw, Feraligatr, Sentret, Furret, Hoothoot, Noctowl, Ledyba, Ledian,
            Spinarak, Ariados, Crobat, Chinchou, Lanturn, Cleffa, Igglybuff, Togepi, Togetic, Natu, Xatu, Mareep, Flaaffy,
            Ampharos, Bellossom, Azumarill, Sudowoodo, Politoed, Hoppip, Skiploom, Jumpluff, Aipom, Yanma, Wooper, Quagsire,
            Espeon, Murkrow, Slowking, Misdreavus, Girafarig, Pineco, Forretress, Dunsparce, Gligar, Steelix, Snubbull, Granbull,
            Qwilfish, Shuckle, Slugma, Magcargo, Swinub, Piloswine, Corsola, Remoraid, Delibird, Mantine, Skarmory, Houndour,
            Houndoom, Kingdra, Phanpy, Donphan, Stantler, Smeargle, Tyrogue, Smoochum, Elekid, Magby, Miltank, Blissey, Larvitar,
            Pupitar, Tyranitar, Torchic, Combusken, Marshtomp, Swampert, Poochyena, Mightyena, Linoone, Wurmple, Beautifly,
            Dustox, Lotad, Lombre, Ludicolo, Seedot, Nuzleaf, Shiftry, Swellow, Wingull, Pelipper, Ralts, Kirlia, Surskit,
            Masquerain, Shroomish, Slakoth, Vigoroth, Slaking, Nincada, Ninjask, Shedinja, Whismur, Exploud, Nosepass, Skitty,
            Aron, Lairon, Aggron, Meditite, Electrike, Manectric, Plusle, Minun, Volbeat, Illumise, Roselia, Gulpin, Swalot,
            Carvanha, Sharpedo, Wailmer, Numel, Camerupt, Torkoal, Spoink, Grumpig, Trapinch, Vibrava, Flygon, Cacnea, Cacturne,
            Swablu, Altaria, Zangoose, Seviper, Lunatone, Solrock, Barboach, Whiscash, Crawdaunt, Baltoy, Claydol, Lileep,
            Cradily, Anorith, Feebas, Shuppet, Banette, Duskull, Dusclops, Tropius, Absol, Snorunt, Glalie, Spheal, Sealeo,
            Walrein, Clamperl, Huntail, Gorebyss, Relicanth, Luvdisc, Bagon, Shelgon, Salamence, Beldum, Metang, Metagross,
            Turtwig, Grotle, Torterra, Monferno, Infernape, Piplup, Prinplup, Empoleon, Starly, Staravia, Staraptor, Bibarel,
            Kricketot, Kricketune, Luxio, Budew, Cranidos, Rampardos, Shieldon, Bastiodon, BurmyPink, BurmyGreen, WormadamBlue,
            WormadamGreen, WormadamPink, Mothim, Combee, Vespiquen, Buizel, Floatzel, Cherubi, CherrimA, ShellosC, ShellosD,
            GastrodonA, GastrodonB, Ambipom, Drifloon, Drifblim, Buneary, Mismagius, Honchkrow, Glameow, Purugly, Chingling,
            Stunky, Skuntank, Bronzor, Bronzong, Bonsly, MimeJr, Happiny, Spiritomb, Gible, Gabite, Garchomp, Munchlax, Riolu,
            Hippopotas, Hippowdon, Skorupi, Drapion, Toxicroak, Carnivine, Finneon, Lumineon, Mantyke, Snover, Abomasnow, Weavile,
            Lickilicky, Tangrowth, Electivire, Magmortar, Yanmega, Leafeon, Glaceon, Gliscor, Mamoswine, Probopass, Rotom
        });

        public bool IsGenderLocked => GenderLocked.Value.Contains(this);
        public bool IsFemaleOnly => FemaleOnly.Value.Contains(this);
        public bool IsValidClient => ValidClients.Value.Contains(this);

        string INameValuePair.Value => this.Value.ToString();

        private Pokemon(string name, uint value) : base(name, value)
        {
        }

        public uint GetGenderSpecificId(bool isFemale)
        {
            if (this.IsGenderLocked) {
                return this.Value;
            }

            return this.Value + (isFemale ? FemaleIdBase : 0);
        }

        public static IEnumerable<Pokemon> ValidList => List.Where(item => item.IsValidClient);

        public static Pokemon FromStringValue(string value)
            => FromStringValueInternal(value, None);

        public static Pokemon FromValueEx(uint value)
            => FromValue(value, None);

        public static Pokemon FromGenderSpecificValue(uint value)
            => FromValue(value % FemaleIdBase, None);
    }
}
