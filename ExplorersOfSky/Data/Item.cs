using System;
using System.Collections.Generic;
using System.Linq;
using ExplorersOfSky.Data.Enums;

namespace ExplorersOfSky.Data
{
    public sealed partial class Item : UInt32EnumBase<Item>, INameValuePair
    {
        private static readonly Lazy<HashSet<Item>> BannedItems = new Lazy<HashSet<Item>>(() => new HashSet<Item> {
            None, None2
        });
        private static readonly Lazy<HashSet<Item>> BannedTargetItems = new Lazy<HashSet<Item>>(() => new HashSet<Item> {
            None, None2, Stick, IronThorn, SilverSpike, GoldFang, GoldThorn
        });

        public bool IsValidReward => !BannedItems.Value.Contains(this);
        public bool IsValidTarget => !BannedTargetItems.Value.Contains(this);

        string INameValuePair.Value => this.Value.ToString();

        private Item(string name, uint value) : base(name, value)
        {
        }

        public static IEnumerable<Item> ValidList => List.Where(item => item.IsValidReward);
        public static IEnumerable<Item> ValidTargetList => List.Where(item => item.IsValidTarget);
        public static Item FromStringValue(string value)
            => FromStringValueInternal(value, None);

        public static Item FromValueEx(uint value)
            => FromValue(value, None);
    }
}
