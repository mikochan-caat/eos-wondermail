namespace ExplorersOfSky.Data
{
    public static class Constants
    {
        public static int MinimumFloorNumber = 1;
        public static int MaximumFloorNumber = 99;
    }
}
