using System;
using System.Collections.Generic;
using System.Linq;
using ExplorersOfSky.Data.Enums;

namespace ExplorersOfSky.Data
{
    public sealed partial class RewardType : UInt32EnumBase<RewardType>, INameValuePair
    {
        private static readonly Lazy<HashSet<RewardType>> ItemRewardTypes =
            new Lazy<HashSet<RewardType>>(() => new HashSet<RewardType> {
                CashWithItem, Item, ItemWithRandom, RewardItem
            });

        public bool IsRewardTypeItem => ItemRewardTypes.Value.Contains(this);

        string INameValuePair.Value => this.Value.ToString();

        private RewardType(string name, uint value) : base(name, value)
        {
        }

        public static IEnumerable<RewardType> ValidList => List.Where(item => item != None);

        public static RewardType FromStringValue(string value)
            => FromStringValueInternal(value, None);

        public static RewardType FromValueEx(uint value)
            => FromValue(value, None);
    }
}
