using System;
using Ardalis.SmartEnum;

namespace ExplorersOfSky.Data.Enums
{
    public abstract class Int32EnumBase<T> : SmartEnum<T, int> where T : Int32EnumBase<T>
    {
        protected Int32EnumBase(string name, int value) : base(name, value)
        {
        }

        protected static T FromStringValueInternal(string value, T defaultValue = null)
        {
            Int32.TryParse(value, out int parseResult);
            return FromValue(parseResult, defaultValue);
        }
    }
}
