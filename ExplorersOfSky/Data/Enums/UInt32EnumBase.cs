using System;
using Ardalis.SmartEnum;

namespace ExplorersOfSky.Data.Enums
{
    public abstract class UInt32EnumBase<T> : SmartEnum<T, uint> where T : UInt32EnumBase<T>
    {
        protected UInt32EnumBase(string name, uint value) : base(name, value)
        {
        }

        protected static T FromStringValueInternal(string value, T defaultValue = null)
        {
            UInt32.TryParse(value, out uint parseResult);
            return FromValue(parseResult, defaultValue);
        }
    }
}
