using System.Collections.Generic;
using ExplorersOfSky.Data.Enums;
using ExplorersOfSky.Data.Missions;

namespace ExplorersOfSky.Data
{
    public sealed partial class Mission : Int32EnumBase<Mission>, INameValuePair
    {
        public MissionType MissionType { get; private set; }

        public IMissionProperties Properties
        {
            get;
            private set;
        }
        public IReadOnlyList<ISubType> SubTypes
        {
            get;
            private set;
        }

        string INameValuePair.Value => this.Value.ToString();

        private Mission(string name, int value) : base(name, value)
        {
            this.SubTypes = new List<SubType>();
        }

        public static Mission FromStringValue(string value)
            => FromStringValueInternal(value);

        public static Mission FromValueEx(int value)
            => FromValue(value);

        public interface ISubType
        {
            string Name { get; }
            IMissionProperties Properties { get; }
        }

        public sealed class SubType : ISubType
        {
            public string Name { get; set; }
            public IMissionProperties Properties
            {
                get;
                set;
            }
        }
    }
}
