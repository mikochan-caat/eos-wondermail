using ExplorersOfSky.Data.Enums;

namespace ExplorersOfSky.Data
{
    public sealed partial class Dungeon : UInt32EnumBase<Dungeon>, INameValuePair
    {
        string INameValuePair.Value => this.Value.ToString();

        private Dungeon(string name, uint value) : base(name, value)
        {
        }

        public static Dungeon FromStringValue(string value)
            => FromStringValueInternal(value);

        public static Dungeon FromValueEx(uint value)
            => FromValue(value);
    }
}
