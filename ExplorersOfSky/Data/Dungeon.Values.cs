namespace ExplorersOfSky.Data
{
    public sealed partial class Dungeon
    {
        public static readonly Dungeon BeachCave = new Dungeon("Beach Cave", 1);
        public static readonly Dungeon DrenchedBluff = new Dungeon("Drenched Bluff", 3);
        public static readonly Dungeon MtBristle = new Dungeon("Mt. Bristle", 4);
        public static readonly Dungeon WaterfallCave = new Dungeon("Waterfall Cave", 6);
        public static readonly Dungeon AppleWoods = new Dungeon("Apple Woods", 7);
        public static readonly Dungeon CraggyCoast = new Dungeon("Craggy Coast", 8);
        public static readonly Dungeon MtHorn = new Dungeon("Mt. Horn", 10);
        public static readonly Dungeon FoggyForest = new Dungeon("Foggy Forest", 12);
        public static readonly Dungeon SteamCave = new Dungeon("Steam Cave", 14);
        public static readonly Dungeon AmpPlains = new Dungeon("Amp Plains", 17);
        public static readonly Dungeon NorthernDesert = new Dungeon("Northern Desert", 20);
        public static readonly Dungeon QuicksandCave = new Dungeon("Quicksand Cave", 21);
        public static readonly Dungeon CrystalCave = new Dungeon("Crystal Cave", 24);
        public static readonly Dungeon CrystalCrossing = new Dungeon("Crystal Crossing", 25);
        public static readonly Dungeon TreeshroudForest = new Dungeon("Treeshroud Forest", 34);
        public static readonly Dungeon BrineCave = new Dungeon("Brine Cave", 35);
        public static readonly Dungeon MystifyingForest = new Dungeon("Mystifying Forest", 44);
        public static readonly Dungeon BlizzardIsland = new Dungeon("Blizzard Island", 46);
        public static readonly Dungeon CreviceCave = new Dungeon("Crevice Cave", 47);
        public static readonly Dungeon SurroundedSea = new Dungeon("Surrounded Sea", 50);
        public static readonly Dungeon MiracleSea = new Dungeon("Miracle Sea", 51);
        public static readonly Dungeon MtTravail = new Dungeon("Mt. Travail", 62);
        public static readonly Dungeon SpacialRift = new Dungeon("Spacial Rift", 64);
        public static readonly Dungeon DarkCrater = new Dungeon("Dark Crater", 67);
        public static readonly Dungeon ConcealedRuins = new Dungeon("Concealed Ruins", 70);
        public static readonly Dungeon MarineResort = new Dungeon("Marine Resort", 72);
        public static readonly Dungeon BottomlessSea = new Dungeon("Bottomless Sea", 73);
        public static readonly Dungeon ShimmerDesert = new Dungeon("Shimmer Desert", 75);
        public static readonly Dungeon MtAvalanche = new Dungeon("Mt. Avalanche", 77);
        public static readonly Dungeon GiantVolcano = new Dungeon("Giant Volcano", 79);
        public static readonly Dungeon WorldAbyss = new Dungeon("World Abyss", 81);
        public static readonly Dungeon SkyStairway = new Dungeon("Sky Stairway", 83);
        public static readonly Dungeon MysteryJungle = new Dungeon("Mystery Jungle", 85);
        public static readonly Dungeon SerenityRiver = new Dungeon("Serenity River", 87);
        public static readonly Dungeon LandslideCave = new Dungeon("Landslide Cave", 88);
        public static readonly Dungeon LushPrairie = new Dungeon("Lush Prairie", 89);
        public static readonly Dungeon TinyMeadow = new Dungeon("Tiny Meadow", 90);
        public static readonly Dungeon LabyrinthCave = new Dungeon("Labyrinth Cave", 91);
        public static readonly Dungeon OranForest = new Dungeon("Oran Forest", 92);
        public static readonly Dungeon LakeAfar = new Dungeon("Lake Afar", 93);
        public static readonly Dungeon HappyOutlook = new Dungeon("Happy Outlook", 94);
        public static readonly Dungeon MtMistral = new Dungeon("Mt. Mistral", 95);
        public static readonly Dungeon ShimmerHill = new Dungeon("Shimmer Hill", 96);
        public static readonly Dungeon LostWilderness = new Dungeon("Lost Wilderness", 97);
        public static readonly Dungeon MidnightForest = new Dungeon("Midnight Forest", 98);
        public static readonly Dungeon ZeroIsleNorth = new Dungeon("Zero Isle North", 99);
        public static readonly Dungeon ZeroIsleEast = new Dungeon("Zero Isle East", 100);
        public static readonly Dungeon ZeroIsleWest = new Dungeon("Zero Isle West", 101);
        public static readonly Dungeon ZeroIsleSouth = new Dungeon("Zero Isle South", 102);
        public static readonly Dungeon ZeroIsleCenter = new Dungeon("Zero Isle Center", 103);
        public static readonly Dungeon OblivionForest = new Dungeon("Oblivion Forest", 107);
        public static readonly Dungeon TreacherousWaters = new Dungeon("Treacherous Waters", 108);
        public static readonly Dungeon SoutheasternIslands = new Dungeon("Southeastern Islands", 109);
        public static readonly Dungeon InfernoCave = new Dungeon("Inferno Cave", 110);
    }
}
