using System;
using System.Windows.Forms;
using ExplorersOfSky.Win32;
using NullGuard;

namespace ExplorersOfSky.UI
{
    public static class MessageDialog
    {
        public static DialogResult Show(string text, Options options) => Show(null, text, options);

        public static DialogResult Show([AllowNull]IWin32Window owner, string text) => Show(owner, text, new Options());

        public static DialogResult Show([AllowNull]IWin32Window owner, string text, Options options)
        {
            if (owner != null) {
                MessageDialogHook.HookNextDialogToOwner(owner);
            }

            return MessageBox.Show(owner, text, options.Caption, options.Buttons, options.Icon, options.DefaultButton);
        }

        public sealed class Options
        {
            public string Caption { get; set; }
            public MessageBoxButtons Buttons { get; set; }
            public MessageBoxDefaultButton DefaultButton { get; set; }
            public MessageBoxIcon Icon { get; set; }

            public Options()
            {
                this.Caption = String.Empty;
                this.Buttons = MessageBoxButtons.OK;
                this.DefaultButton = MessageBoxDefaultButton.Button1;
                this.Icon = MessageBoxIcon.None;
            }
        }
    }
}
