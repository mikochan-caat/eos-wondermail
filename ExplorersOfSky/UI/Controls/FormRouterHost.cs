using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Forms;
using ExplorersOfSky.Services;
using NullGuard;
using Splat;

namespace ExplorersOfSky.UI.Controls
{
    public sealed partial class FormRouterHost : Component
    {
        private IFormRouter router;
        private IDisposable routerSubscription;

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [AllowNull]
        public IFormRouter Router
        {
            get => this.router;
            set
            {
                if (!this.DesignMode && value != null) {
                    this.routerSubscription?.Dispose();

                    this.router = value;
                    this.routerSubscription = this.router.NavigationChanged.Subscribe(this.OnNavigationChanged);
                } else {
                    this.router = null;
                }
            }
        }

        public ContainerControl ContainerControl { get; set; }

        [AllowNull]
        public override ISite Site
        {
            get => base.Site;
            set
            {
                base.Site = value;

                var host = value?.GetService(typeof(IDesignerHost)) as IDesignerHost;
                var componentHost = host?.RootComponent;
                if (componentHost is ContainerControl containerControl) {
                    this.ContainerControl = containerControl;
                }
            }
        }

        private Form OwnerForm => this.ContainerControl as Form;

        public FormRouterHost() : this(null)
        {
        }

        public FormRouterHost([AllowNull]IContainer container)
        {
            container?.Add(this);
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                this.components?.Dispose();
                this.routerSubscription?.Dispose();
            }

            base.Dispose(disposing);
        }

        private void OnNavigationChanged(FormRouter.IRouteMessage message)
        {
            if (this.ContainerControl == null) {
                throw new InvalidOperationException("This Form Router Host must have a container control.");
            }

            var vmService = Locator.Current.GetService<IViewModelService>();
            var form = vmService.GetViewAsFormFor(message.ViewModel);

            this.ContainerControl.Invoke(new MethodInvoker(() => {
                if (message is FormRouter.NavigationMessage navigateMessage) {
                    using (form) {
                        if (navigateMessage.IsModal) {
                            form.ShowDialog(this.OwnerForm);
                        } else {
                            this.InitializeRouterNavigateEvents(form);
                            this.OwnerForm.Hide();
                            form.ShowDialog(this.OwnerForm);
                        }
                    }
                } else {
                    throw new InvalidOperationException($"Unsupported route message type: {message.GetType().FullName}");
                }
            }));
        }

        private void InitializeRouterNavigateEvents(Form form)
        {
            var disposables = new CompositeDisposable();
            Observable.FromEventPattern(form, nameof(form.Disposed))
                .Subscribe(_ => {
                    // Attempt to workaround composite window bug
                    var currentWindowState = this.OwnerForm.WindowState;
                    this.OwnerForm.WindowState = FormWindowState.Minimized;
                    this.OwnerForm.Show();
                    this.OwnerForm.WindowState = currentWindowState;

                    disposables.Dispose();
                })
                .DisposeWith(disposables);
        }
    }
}
