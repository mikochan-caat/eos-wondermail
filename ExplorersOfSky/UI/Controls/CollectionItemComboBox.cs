using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Forms;

namespace ExplorersOfSky.UI.Controls
{
    public partial class CollectionItemComboBox : ComboBox
    {
        private readonly CompositeDisposable disposables;

        private string lastSelectedText;

        public CollectionItemComboBox()
        {
            this.disposables = new CompositeDisposable();

            this.InitializeComponent();
            this.InitializeDesign();
            this.InitializeEvents();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (this.components != null) {
                    this.components.Dispose();
                }

                this.disposables.Dispose();
            }

            base.Dispose(disposing);
        }

        protected override void OnSelectedValueChanged(EventArgs e)
        {
            base.OnSelectedValueChanged(e);
            this.lastSelectedText = this.Text;
        }

        private void InitializeDesign()
        {
            this.DoubleBuffered = true;
            this.ConfigureDataMemberBindings();
            this.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void InitializeEvents()
        {
            Observable.FromEventPattern(this, nameof(this.Leave))
                .Subscribe(_ => this.SetTextToLastSelected())
                .DisposeWith(this.disposables);
            Observable.FromEventPattern(this, nameof(this.DropDown))
                .Subscribe(_ => this.SetTextToLastSelected())
                .DisposeWith(this.disposables);
            Observable.FromEventPattern(this, nameof(this.KeyDown))
                .Subscribe(_ => this.DroppedDown = false)
                .DisposeWith(this.disposables);
        }

        private void SetTextToLastSelected() => this.Text = this.lastSelectedText;
    }
}
