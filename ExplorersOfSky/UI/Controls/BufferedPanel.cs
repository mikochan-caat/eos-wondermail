using System.Windows.Forms;

namespace ExplorersOfSky.UI.Controls
{
    public class BufferedPanel : Panel
    {
        public BufferedPanel()
        {
            this.DoubleBuffered = true;
        }
    }
}
