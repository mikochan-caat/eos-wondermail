using System;
using System.Reactive;
using System.Windows.Forms;
using ExplorersOfSky.UI.Notifications;
using ReactiveUI;

namespace ExplorersOfSky.UI.Controls
{
    public partial class BaseForm : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                if (!this.DesignMode) {
                    cp.ExStyle |= 0x02000000;
                }

                return cp;
            }
        }

        protected BaseForm()
        {
            this.InitializeComponent();
            this.InitializeDesign();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null) {
                this.components.Dispose();
            }
            if (disposing) {
                this.OnComponentDisposing();
            }

            base.Dispose(disposing);
        }

        protected virtual void OnComponentDisposing()
        {
        }

        protected void DisplayNotificationDefault(InteractionContext<NotificationMessage, Unit> context)
        {
            this.Invoke(new MethodInvoker(() => {
                MessageDialog.Show(this, context.Input.Text, new MessageDialog.Options {
                    Caption = context.Input.Title,
                    Buttons = MessageBoxButtons.OK,
                    Icon = NotificationTypeToIcon(context.Input.Type)
                });
                context.SetOutput(Unit.Default);
            }));
        }

        protected void DisplayConfirmationDefault(InteractionContext<NotificationMessage, ConfirmationResult> context)
            => this.DisplayConfirmationDefault(context, context => new MessageDialog.Options {
                Caption = context.Input.Title,
                Buttons = MessageBoxButtons.YesNo,
                DefaultButton = MessageBoxDefaultButton.Button2,
                Icon = NotificationTypeToIcon(context.Input.Type)
            });

        protected void DisplayConfirmationDefault(
            InteractionContext<NotificationMessage, ConfirmationResult> context,
            Func<InteractionContext<NotificationMessage, ConfirmationResult>, MessageDialog.Options> configure)
        {
            this.Invoke(new MethodInvoker(() => {
                var result = MessageDialog.Show(this, context.Input.Text, configure(context));
                context.SetOutput(DialogResultToConfirmationResult(result));
            }));
        }

        private void InitializeDesign()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
            this.UpdateStyles();
        }

        protected static MessageBoxIcon NotificationTypeToIcon(NotificationType level)
        {
            return level switch {
                NotificationType.Info => MessageBoxIcon.Information,
                NotificationType.Warning => MessageBoxIcon.Warning,
                NotificationType.Error => MessageBoxIcon.Error,
                NotificationType.Question => MessageBoxIcon.Question,
                _ => MessageBoxIcon.None,
            };
        }

        protected static ConfirmationResult DialogResultToConfirmationResult(DialogResult result)
        {
            return result switch {
                DialogResult.OK => ConfirmationResult.OK,
                DialogResult.Cancel => ConfirmationResult.Cancel,
                DialogResult.Abort => ConfirmationResult.Abort,
                DialogResult.Retry => ConfirmationResult.Retry,
                DialogResult.Ignore => ConfirmationResult.Ignore,
                DialogResult.Yes => ConfirmationResult.Yes,
                DialogResult.No => ConfirmationResult.No,
                _ => ConfirmationResult.OK
            };
        }
    }
}
