using System.Reflection;
using System.Windows.Forms;

namespace ExplorersOfSky.UI.Controls
{
    public static class ControlExtensions
    {
        public static void ForceDoubleBuffer(this Control control, bool enable)
        {
            if (SystemInformation.TerminalServerSession) {
                return;
            }

            var bindingFlags = BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;
            var property = typeof(Control).GetProperty("DoubleBuffered", bindingFlags);
            property?.SetValue(control, enable);
        }
    }
}
