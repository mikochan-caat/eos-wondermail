using System;

namespace ExplorersOfSky.UI.Notifications
{
    public sealed class NotificationMessage
    {
        public string Identifier { get; set; }
        public NotificationType Type { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }

        public NotificationMessage()
        {
            this.Identifier = String.Empty;
            this.Title = String.Empty;
        }
    }
}
