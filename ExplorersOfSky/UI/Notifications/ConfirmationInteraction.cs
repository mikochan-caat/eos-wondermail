using ReactiveUI;

namespace ExplorersOfSky.UI.Notifications
{
    public sealed class ConfirmationInteraction : Interaction<NotificationMessage, ConfirmationResult>
    {
        public ConfirmationInteraction() : base(null)
        {
        }
    }
}
