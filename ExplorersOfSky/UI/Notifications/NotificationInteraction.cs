using System.Reactive;
using ReactiveUI;

namespace ExplorersOfSky.UI.Notifications
{
    public sealed class NotificationInteraction : Interaction<NotificationMessage, Unit>
    {
        public NotificationInteraction() : base(null)
        {
        }
    }
}
