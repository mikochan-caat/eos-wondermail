namespace ExplorersOfSky.UI.Notifications
{
    public enum NotificationType
    {
        Info,
        Warning,
        Error,
        Question
    }
}
