namespace ExplorersOfSky.UI.Notifications
{
    public enum ConfirmationResult
    {
        OK,
        Cancel,
        Abort,
        Retry,
        Ignore,
        Yes,
        No
    }
}
