using System;
using System.Text;

namespace ExplorersOfSky.UI
{
    public static class PrettyWonderMail
    { 
        private const int WonderMailLength = 34;
        private const int DisplayRows = 2;
        private const int DisplayMiddleColumnSize = 7;

        public static string Format(string mailString)
        {
            if (String.IsNullOrEmpty(mailString) || mailString.Length < WonderMailLength) {
                return mailString;
            }

            int outerColumnSize = (mailString.Length - (DisplayRows * DisplayMiddleColumnSize)) / (DisplayRows * 2);

            var builder = new StringBuilder();
            int stringPtr = 0;
            for (int row = 0; row < DisplayRows; row++) {
                if (builder.Length > 0) {
                    builder.AppendLine();
                }

                builder.Append($"{mailString.Substring(stringPtr, outerColumnSize)} ");
                stringPtr += outerColumnSize;

                builder.Append($"{mailString.Substring(stringPtr, DisplayMiddleColumnSize)} ");
                stringPtr += DisplayMiddleColumnSize;

                builder.Append($"{mailString.Substring(stringPtr, outerColumnSize)}");
                stringPtr += outerColumnSize;
            }

            return builder.ToString();
        }
    }
}
