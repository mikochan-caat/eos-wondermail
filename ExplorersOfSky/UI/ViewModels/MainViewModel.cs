using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using ExplorersOfSky.Data;
using ExplorersOfSky.Data.Models;
using ExplorersOfSky.UI.Notifications;
using ExplorersOfSky.UI.ViewModels.Extensible;
using NullGuard;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace ExplorersOfSky.UI.ViewModels
{
    public sealed class MainViewModel : BaseViewModel
    {
        private readonly Dictionary<(FieldType type, bool valid), IList<CollectionItem>> validListCache;

        [Reactive] public Mission SelectedMission { get; set; }
        [Reactive] public int SelectedMissionSubType { get; set; }
        [Reactive] public Dungeon SelectedDungeon { get; set; }
        [Reactive] public int InputtedFloorNumber { get; set; }
        [Reactive] public Pokemon SelectedClient { get; set; }
        [Reactive] public Pokemon SelectedTarget { get; set; }
        [Reactive] public Item SelectedTargetItem { get; set; }
        [Reactive] public RewardType SelectedRewardType { get; set; }
        [Reactive] public Item SelectedRewardItem { get; set; }
        [Reactive] public bool IsClientFemale { get; set; }
        [Reactive] public bool IsTargetFemale { get; set; }
        [Reactive] public bool IsEuroWonderMail { get; set; }
        [Reactive] public string CreatedWonderMailCode { get; set; }
        [Reactive] public bool IsValidWonderMail { get; set; }

        public IEnumerable<CollectionItem> Missions { get; private set; }
        public IEnumerable<CollectionItem> Dungeons { get; private set; }
        [Reactive] public IEnumerable<CollectionItem> ClientList { get; private set; }
        [Reactive] public IEnumerable<CollectionItem> TargetList { get; private set; }
        [Reactive] public IEnumerable<CollectionItem> TargetItems { get; private set; }
        [Reactive] public IEnumerable<CollectionItem> RewardTypes { get; private set; }
        [Reactive] public IEnumerable<CollectionItem> RewardItems { get; private set; }

        public extern IEnumerable<CollectionItem> MissionSubTypes { [ObservableAsProperty]get; }
        public extern bool IsMissionSubTypesVisible { [ObservableAsProperty]get; }
        public extern bool IsClientSelectable { [ObservableAsProperty]get; }
        public extern bool IsTargetSelectable { [ObservableAsProperty]get; }
        public extern bool IsClientGenderSelectable { [ObservableAsProperty]get; }
        public extern bool IsTargetGenderSelectable { [ObservableAsProperty]get; }
        public extern bool CanSelectTargetItem { [ObservableAsProperty]get; }
        public extern bool CanSelectReward { [ObservableAsProperty]get; }
        public extern bool CanSelectRewardItem { [ObservableAsProperty]get; }

        public ReactiveCommand<Unit, Unit> CreateWonderMail { get; private set; }
        public ReactiveCommand<Unit, Unit> GetARCode { get; private set; }
        public ReactiveCommand<Unit, Task> DisplayAppInfo { get; private set; }

        public MainViewModel()
        {
            this.validListCache = new Dictionary<(FieldType, bool), IList<CollectionItem>>();

            this.WhenActivated((disposables) => {
                this.InitializeViewModel(disposables);
            });
        }

        private void InitializeViewModel(CompositeDisposable disposables)
        {
            this.Title = $"v{this.AppInfo.FileVersion}";

            this.SelectedMission = Mission.RescueClient;
            this.SelectedMissionSubType = -1;
            this.SelectedDungeon = Dungeon.BeachCave;
            this.InputtedFloorNumber = 1;
            this.SelectedClient = Pokemon.Bulbasaur;
            this.SelectedTarget = Pokemon.Bulbasaur;
            this.SelectedTargetItem = Item.Apple;
            this.SelectedRewardType = RewardType.Cash;
            this.SelectedRewardItem = Item.Apple;
            this.IsClientFemale = false;
            this.IsTargetFemale = false;
            this.IsEuroWonderMail = false;
            this.CreatedWonderMailCode = String.Empty;

            this.validListCache[(FieldType.Client, false)] =
                CollectionItem.FromNameValues(Pokemon.List.OrderBy(item => item.Value));
            this.validListCache[(FieldType.Client, true)] =
                CollectionItem.FromNameValues(Pokemon.ValidList.OrderBy(item => item.Value));
            this.validListCache[(FieldType.Target, false)] =
                CollectionItem.FromNameValues(Pokemon.List.OrderBy(item => item.Value));
            this.validListCache[(FieldType.Target, true)] =
                CollectionItem.FromNameValues(Pokemon.ValidList.OrderBy(item => item.Value));
            this.validListCache[(FieldType.TargetItem, false)] =
                CollectionItem.FromNameValues(Item.List.OrderBy(item => item.Value));
            this.validListCache[(FieldType.TargetItem, true)] =
                CollectionItem.FromNameValues(Item.ValidTargetList.OrderBy(item => item.Value));
            this.validListCache[(FieldType.RewardType, false)] =
                CollectionItem.FromNameValues(RewardType.List.OrderBy(item => item.Value));
            this.validListCache[(FieldType.RewardType, true)] =
                CollectionItem.FromNameValues(RewardType.ValidList.OrderBy(item => item.Value));
            this.validListCache[(FieldType.RewardItem, false)] =
                CollectionItem.FromNameValues(Item.List.OrderBy(item => item.Value));
            this.validListCache[(FieldType.RewardItem, true)] =
                CollectionItem.FromNameValues(Item.ValidList.OrderBy(item => item.Value));

            this.Missions = CollectionItem.FromNameValues(Mission.List.OrderBy(item => item.Value));
            this.Dungeons = CollectionItem.FromNameValues(Dungeon.List.OrderBy(item => item.Value));
            this.ClientList = this.validListCache[(FieldType.Client, true)];
            this.TargetList = this.validListCache[(FieldType.Target, true)];
            this.TargetItems = this.validListCache[(FieldType.TargetItem, true)];
            this.RewardTypes = this.validListCache[(FieldType.RewardType, true)];
            this.RewardItems = this.validListCache[(FieldType.RewardItem, true)];

            this.BindMissionSubTypes(disposables);
            this.BindMissionDependents(disposables);
            this.BindIsClientGenderSelectable(disposables);
            this.BindIsTargetGenderSelectable(disposables);
            this.BindCanSelectTargetItem(disposables);
            this.BindCanSelectRewardItem(disposables);
            this.BindCommands(disposables);
        }

        private void BindMissionSubTypes(CompositeDisposable disposables)
        {
            this.WhenAnyValue(vm => vm.SelectedMission)
                .Select(mission => mission.SubTypes)
                .Select(subTypes => subTypes.Select((subtype, index) => new { subtype.Name, Index = index.ToString() }))
                .Select(subTypes => CollectionItem.FromEnumerable(subTypes, item => item.Name, item => item.Index, false))
                .ToPropertyEx(this, vm => vm.MissionSubTypes)
                .DisposeWith(disposables);

            this.WhenAnyValue(vm => vm.MissionSubTypes)
                .Select(subTypes => subTypes?.Any() ?? false)
                .ToPropertyEx(this, vm => vm.IsMissionSubTypesVisible)
                .DisposeWith(disposables);
        }

        private void BindMissionDependents(CompositeDisposable disposables)
        {
            var source = this.WhenAnyValue(vm => vm.SelectedMission, vm => vm.SelectedMissionSubType)
                .Select(obs => new {
                    Mission = obs.Item1,
                    SubType = obs.Item1.SubTypes.ElementAtOrDefault(obs.Item2)
                });

            source
                .Select(obj => !this.IsForcedClientSelector(obj.Mission, obj.SubType))
                .ToPropertyEx(this, vm => vm.IsClientSelectable)
                .DisposeWith(disposables);
            source
                .Select(obj => !this.IsForcedTargetSelector(obj.Mission, obj.SubType))
                .ToPropertyEx(this, vm => vm.IsTargetSelectable)
                .DisposeWith(disposables);
            source
                .Select(obj => obj.Mission.Properties.UseTargetItem || (obj.SubType?.Properties.UseTargetItem ?? false))
                .ToPropertyEx(this, vm => vm.CanSelectTargetItem)
                .DisposeWith(disposables);
            source
                .Select(obj => !obj.Mission.Properties.HasNoReward && !(obj.SubType?.Properties.HasNoReward ?? false))
                .ToPropertyEx(this, vm => vm.CanSelectReward)
                .DisposeWith(disposables);

            bool isClientSelfUpdating = false;
            this.WhenAnyValue(vm => vm.SelectedClient)
                .CombineLatest(source)
                .Where(_ => !isClientSelfUpdating)
                .Subscribe(obs => {
                    isClientSelfUpdating = true;

                    var missionInfo = obs.Second;
                    bool isForcedClient = this.IsForcedClientSelector(missionInfo.Mission, missionInfo.SubType);
                    Pokemon forcedClient = null;
                    if (isForcedClient) {
                        if (missionInfo.SubType?.Properties.HasForcedClient ?? false) {
                            forcedClient = missionInfo.SubType.Properties.ForcedClient;
                        } else {
                            forcedClient = missionInfo.Mission.Properties.ForcedClient;
                        }
                    }

                    this.ClientList = this.validListCache[(FieldType.Client, !isForcedClient)];
                    if (isForcedClient) {
                        this.SelectedClient = forcedClient;
                    }

                    isClientSelfUpdating = false;
                })
                .DisposeWith(disposables);

            bool isTargetSelfUpdating = false;
            this.WhenAnyValue(vm => vm.SelectedTarget, vm => vm.SelectedClient, vm => vm.IsClientFemale)
                .CombineLatest(source)
                .Where(_ => !isTargetSelfUpdating)
                .Subscribe(obs => {
                    isTargetSelfUpdating = true;

                    var missionInfo = obs.Second;
                    bool isForcedTarget = this.IsForcedTargetSelector(missionInfo.Mission, missionInfo.SubType);
                    Pokemon forcedTarget = null;
                    if (isForcedTarget) {
                        if (missionInfo.SubType?.Properties.HasForcedTarget ?? false) {
                            forcedTarget = missionInfo.SubType.Properties.ForcedTarget;
                        } else if (missionInfo.Mission.Properties.HasForcedTarget) {
                            forcedTarget = missionInfo.Mission.Properties.ForcedTarget;
                        } else {
                            forcedTarget = this.SelectedClient;
                        }
                    }

                    this.TargetList = this.validListCache[(FieldType.Target, !isForcedTarget)];
                    if (isForcedTarget) {
                        this.SelectedTarget = forcedTarget;
                        if (!forcedTarget.IsGenderLocked) {
                            this.IsTargetFemale = this.IsClientFemale;
                        }
                    }

                    isTargetSelfUpdating = false;
                })
                .DisposeWith(disposables);
        }

        private void BindIsClientGenderSelectable(CompositeDisposable disposables)
        {
            this.WhenAnyValue(vm => vm.IsClientSelectable, vm => vm.SelectedClient)
                .Select(obs => obs.Item1 && !obs.Item2.IsGenderLocked)
                .ToPropertyEx(this, vm => vm.IsClientGenderSelectable)
                .DisposeWith(disposables);

            this.WhenAnyValue(vm => vm.IsClientGenderSelectable, vm => vm.SelectedClient)
                .Where(obs => !obs.Item1)
                .Subscribe(obs => this.IsClientFemale = obs.Item2.IsFemaleOnly)
                .DisposeWith(disposables);
        }

        private void BindIsTargetGenderSelectable(CompositeDisposable disposables)
        {
            this.WhenAnyValue(vm => vm.IsTargetSelectable, vm => vm.SelectedTarget)
                .Select(obs => obs.Item1 && !obs.Item2.IsGenderLocked)
                .ToPropertyEx(this, vm => vm.IsTargetGenderSelectable)
                .DisposeWith(disposables);

            this.WhenAnyValue(vm => vm.IsTargetGenderSelectable, vm => vm.SelectedTarget)
                .Where(obs => !obs.Item1)
                .Subscribe(obs => this.IsTargetFemale = obs.Item2.IsFemaleOnly)
                .DisposeWith(disposables);
        }

        private void BindCanSelectTargetItem(CompositeDisposable disposables)
        {
            this.WhenAnyValue(vm => vm.CanSelectTargetItem)
                .DistinctUntilChanged()
                .Subscribe(selectable => {
                    this.TargetItems = this.validListCache[(FieldType.TargetItem, selectable)];
                    this.SelectedTargetItem = selectable ? Item.Apple : Item.None;
                })
                .DisposeWith(disposables);
        }

        private void BindCanSelectRewardItem(CompositeDisposable disposables)
        {
            this.WhenAnyValue(vm => vm.CanSelectReward, vm => vm.SelectedRewardType)
                .Select(obs => obs.Item1 && obs.Item2.IsRewardTypeItem)
                .ToPropertyEx(this, vm => vm.CanSelectRewardItem)
                .DisposeWith(disposables);

            this.WhenAnyValue(vm => vm.CanSelectReward)
                .Subscribe(selectable => {
                    this.RewardTypes = this.validListCache[(FieldType.RewardType, selectable)];
                    this.SelectedRewardType = selectable ? RewardType.Cash : RewardType.None;
                })
                .DisposeWith(disposables);

            this.WhenAnyValue(vm => vm.CanSelectRewardItem)
                .Subscribe(selectable => {
                    this.RewardItems = this.validListCache[(FieldType.RewardItem, selectable)];
                    this.SelectedRewardItem = selectable ? Item.Apple : Item.None;
                })
                .DisposeWith(disposables);
        }

        private bool IsForcedClientSelector(Mission mission, [AllowNull]Mission.ISubType subType)
            => mission.Properties.HasForcedClient || (subType?.Properties.HasForcedClient ?? false);

        private bool IsForcedTargetSelector(Mission mission, [AllowNull] Mission.ISubType subType)
            => (mission.Properties.HasForcedTarget || (subType?.Properties.HasForcedTarget ?? false)) ||
               (mission.Properties.IsClientTarget || (subType?.Properties.IsClientTarget ?? false));

        private void BindCommands(CompositeDisposable disposables)
        {
            this.CreateWonderMail = ReactiveCommand
                .CreateFromTask(this.ExecuteGenerateWonderMail)
                .DisposeWith(disposables);

            var getARCodeExecutable = this
                .WhenAnyValue(vm => vm.IsValidWonderMail)
                .CombineLatest(this.CreateWonderMail.IsExecuting)
                .Select(obs => obs.First && !obs.Second);
            this.GetARCode = ReactiveCommand
                .CreateFromTask(this.ExecuteGetARCode, getARCodeExecutable)
                .DisposeWith(disposables);

            this.DisplayAppInfo = ReactiveCommand
                .Create(this.ExecuteDisplayAppInfo)
                .DisposeWith(disposables);
        }

        private async Task ExecuteGenerateWonderMail()
        {
            var mission = this.SelectedMission;
            var missionProps = mission.SubTypes.Count > 0 ?
                                mission.SubTypes[this.SelectedMissionSubType].Properties : mission.Properties;

            var rng = new Random();
            var wonderMail = new WonderMail {
                Mission = (uint)mission.MissionType,
                MissionSubType = (uint)missionProps.SpecialType,
                Dungeon = this.SelectedDungeon,
                FloorNumber = (uint)this.InputtedFloorNumber,
                SpecialFloor = 0,
                RewardType = this.SelectedRewardType,
                RewardItem = Item.Apple,
                TargetItem = Item.Apple,

                UseEUByteSwap = this.IsEuroWonderMail,
                ValidateClient = true,
                ValidateRewardItem = true,
                HasNoReward = false
            };

            if (this.IsClientSelectable) {
                wonderMail.Client = this.SelectedClient.GetGenderSpecificId(this.IsClientFemale);
            } else {
                wonderMail.Client = missionProps.ForcedClient;
                wonderMail.ValidateClient = false;
            }

            if (missionProps.IsClientTarget) {
                wonderMail.Target = wonderMail.Client;
            } else if (this.IsTargetSelectable) {
                wonderMail.Target = this.SelectedTarget.GetGenderSpecificId(this.IsTargetFemale);
            } else {
                wonderMail.Target = missionProps.ForcedTarget;
            }

            if (missionProps.HasNoReward) {
                wonderMail.HasNoReward = true;
                wonderMail.RewardType = RewardType.CashWithItem;
            } else {
                switch (this.SelectedRewardType) {
                    case var type when type == RewardType.CashWithItem || type == RewardType.Item ||
                                       type == RewardType.ItemWithRandom || type == RewardType.RewardItem:
                        wonderMail.RewardItem = this.SelectedRewardItem;
                        break;
                    case var type when type == RewardType.Egg || type == RewardType.ClientJoins:
                        wonderMail.RewardItem = wonderMail.Client;
                        wonderMail.ValidateRewardItem = false;
                        break;
                }
            }

            if (missionProps.UseTargetItem) {
                wonderMail.TargetItem = this.SelectedTargetItem;
            }

            if (missionProps.SpecialFloorLayouts.Count > 0) {
                int randomLayout = rng.Next(0, missionProps.SpecialFloorLayouts.Count);
                wonderMail.SpecialFloor = missionProps.SpecialFloorLayouts[randomLayout];
            }

            var validateResult = wonderMail.Validate();
            this.IsValidWonderMail = validateResult.IsValid;

            if (validateResult.IsValid) {
                this.CreatedWonderMailCode = wonderMail.Generate();
            } else {
                this.CreatedWonderMailCode = validateResult.Summarize("Could not generate a Wonder Mail.");
            }

            await Task.CompletedTask;
        }

        private async Task ExecuteGetARCode()
            => await this.FormRouter.Navigate(new ARCodeViewModel {
                WonderMailCode = this.CreatedWonderMailCode
            }, true);

        private async Task ExecuteDisplayAppInfo()
            => await this.CommonEventsPublisher.NotifyAndWait(new NotificationMessage {
                Title = "Application Information",
                Text = $"{this.AppInfo.ProductName} v{this.AppInfo.FileVersion}\n" +
                       $"   build {this.AppInfo.Build}\n" +
                       $"   {this.AppInfo.Copyright}\n\n" + 
                       $"{this.AppInfo.Description}\n\n" +
                        "Based from the online WMS generator at:\n" +
                        "   https://dengler9.github.io/wmsgenerator"
            });

        private enum FieldType
        {
            Client,
            Target,
            TargetItem,
            RewardType,
            RewardItem
        }
    }
}
