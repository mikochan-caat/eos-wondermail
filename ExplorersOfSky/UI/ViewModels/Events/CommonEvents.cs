using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using ExplorersOfSky.UI.Notifications;

namespace ExplorersOfSky.UI.ViewModels.Events
{
    public interface ICommonEvents
    {
        NotificationInteraction NotificationHandler { get; }
        ConfirmationInteraction ConfirmationHandler { get; }
        IObservable<Unit> OnRequestClose { get; }
    }

    public interface ICommonEventsPublisher
    {
        void RequestClose();
        Task NotifyAndWait(NotificationMessage message);
        Task<ConfirmationResult> ConfirmAndWait(NotificationMessage message);
    }

    public sealed class CommonEvents : ICommonEvents, ICommonEventsPublisher
    {
        private readonly Subject<Unit> requestCloseSubject;

        public NotificationInteraction NotificationHandler { get; }
        public ConfirmationInteraction ConfirmationHandler { get; }
        public IObservable<Unit> OnRequestClose => this.requestCloseSubject.AsObservable();

        public CommonEvents()
        {
            this.NotificationHandler = new NotificationInteraction();
            this.ConfirmationHandler = new ConfirmationInteraction();
            this.requestCloseSubject = new Subject<Unit>();
        }

        public void RequestClose()
            => this.requestCloseSubject.OnNext(Unit.Default);
        public async Task NotifyAndWait(NotificationMessage message)
            => await this.NotificationHandler.Handle(message);
        public async Task<ConfirmationResult> ConfirmAndWait(NotificationMessage message)
            => await this.ConfirmationHandler.Handle(message);
    }
}
