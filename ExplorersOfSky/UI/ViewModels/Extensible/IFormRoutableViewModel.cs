namespace ExplorersOfSky.UI.ViewModels.Extensible
{
    public interface IFormRoutableViewModel
    {
        IFormRouter FormRouter { get; }
    }
}
