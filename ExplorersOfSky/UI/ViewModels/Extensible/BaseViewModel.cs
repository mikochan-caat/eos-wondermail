using System;
using System.Drawing;
using ExplorersOfSky.Properties;
using ExplorersOfSky.UI.ViewModels.Events;
using ExplorersOfSky.Utilities;
using ReactiveUI;

namespace ExplorersOfSky.UI.ViewModels.Extensible
{
    public abstract partial class BaseViewModel : ReactiveObject, IActivatableViewModel, IFormRoutableViewModel
    {
        private static readonly Icon StaticAppIcon = Resources.AppIconIco;

        private readonly Lazy<CommonEvents> commonEventsLazy;
        private readonly ViewModelActivator viewModelActivator;

        private string title;
        private bool prependTitle;

        public Icon AppIcon => StaticAppIcon;
        public string Title
        {
            get
            {
                string dash = this.PrependTitle ? " - " : String.Empty;
                string subtitle = !String.IsNullOrWhiteSpace(this.title) ? $"{dash}{this.title}" : String.Empty;
                return $"{(this.PrependTitle ? this.AppInfo.ProductName : String.Empty)}{subtitle}";
            }
            set => this.RaiseAndSetIfChanged(ref this.title, value);
        }

        public IFormRouter FormRouter { get; }
        public ICommonEvents CommonEvents => this.commonEventsLazy.Value;

        protected bool PrependTitle
        {
            get => this.prependTitle;
            set
            {
                this.prependTitle = value;
                this.RaisePropertyChanged(nameof(this.Title));
            }
        }
        protected AssemblyExtensions.AppInfo AppInfo { get; }
        protected ICommonEventsPublisher CommonEventsPublisher => this.commonEventsLazy.Value;

        ViewModelActivator IActivatableViewModel.Activator => this.viewModelActivator;

        public BaseViewModel()
        {
            this.PrependTitle = true;
            this.AppInfo = typeof(BaseViewModel).Assembly.GetAppInfo();
            this.FormRouter = new FormRouter();
            this.commonEventsLazy = new Lazy<CommonEvents>(() => new CommonEvents(), true);
            this.viewModelActivator = new ViewModelActivator();
        }
    }
}
