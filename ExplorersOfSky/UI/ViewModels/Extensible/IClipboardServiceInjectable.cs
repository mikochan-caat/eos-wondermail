using ExplorersOfSky.Services;

namespace ExplorersOfSky.UI.ViewModels.Extensible
{
    public interface IClipboardServiceInjectable
    {
        IClipboardService ClipboardService { get; set; }
    }
}
