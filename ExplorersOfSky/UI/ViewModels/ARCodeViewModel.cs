using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using ExplorersOfSky.Data.Models;
using ExplorersOfSky.Services;
using ExplorersOfSky.UI.Notifications;
using ExplorersOfSky.UI.ViewModels.Extensible;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace ExplorersOfSky.UI.ViewModels
{
    public sealed class ARCodeViewModel : BaseViewModel, IClipboardServiceInjectable
    {
        private static readonly string ConfirmationIdentifier = "CONFIRMATION";

        public IClipboardService ClipboardService { get; set; }

        [Reactive] public string WonderMailCode { get; set; }
        [Reactive] public string CreatedARCode { get; set; }
        [Reactive] public bool IsARCodeValid { get; set; }

        public ReactiveCommand<Unit, Task> PromptOnLoad { get; private set; }

        public ARCodeViewModel()
        {
            this.PrependTitle = false;

            this.WhenActivated((disposables) => {
                this.InitializeViewModel(disposables);
            });
        }

        private void InitializeViewModel(CompositeDisposable disposables)
        {
            this.Title = "AR Code Generator";

            var arCode = new ARCode { WonderMailCode = this.WonderMailCode };
            var validationResult = arCode.Validate();
            this.IsARCodeValid = validationResult.IsValid;

            if (validationResult.IsValid) {
                this.CreatedARCode = arCode.Generate();
            } else {
                this.CreatedARCode = validationResult.Summarize("Could not generate an AR code!");
            }

            this.BindCommands(disposables);
        }

        private void BindCommands(CompositeDisposable disposables)
        {
            this.PromptOnLoad = ReactiveCommand
                .Create(this.ExecutePromptOnLoad)
                .DisposeWith(disposables);
        }

        private async Task ExecutePromptOnLoad()
        {
            if (this.IsARCodeValid) {
                this.ClipboardService.SetData(this.CreatedARCode);

                var result = await this.CommonEventsPublisher.ConfirmAndWait(new NotificationMessage {
                    Identifier = ConfirmationIdentifier,
                    Text = "AR code copied to clipboard!\n\n" +
                           "You can click OK to head back immediately, or click Cancel for more information."
                });
                if (result == ConfirmationResult.OK) {
                    this.CommonEventsPublisher.RequestClose();
                }
            }
        }
    }
}
