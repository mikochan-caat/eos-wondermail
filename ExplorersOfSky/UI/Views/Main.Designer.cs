
namespace ExplorersOfSky.UI.Views
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.appInfoLink = new System.Windows.Forms.LinkLabel();
            this.mainPanel = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.formLayout = new System.Windows.Forms.TableLayoutPanel();
            this.panel19 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.createWonderMail = new System.Windows.Forms.Button();
            this.panel22 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.extraTargetSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel21 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.missionExtraSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.missionTypeSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel2 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.dungeonSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel5 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.floorNumber = new System.Windows.Forms.NumericUpDown();
            this.panel7 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.clientFemaleCheck = new System.Windows.Forms.CheckBox();
            this.clientSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel9 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel10 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.targetFemaleCheck = new System.Windows.Forms.CheckBox();
            this.targetSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel17 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel15 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel18 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.eurWonderMailCheck = new System.Windows.Forms.CheckBox();
            this.panel13 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel16 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.rewardItemSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel11 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel14 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.rewardTypeSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.panel12 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.targetItemSelect = new ExplorersOfSky.UI.Controls.CollectionItemComboBox();
            this.bufferedPanel1 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.createARCode = new System.Windows.Forms.Button();
            this.panel20 = new ExplorersOfSky.UI.Controls.BufferedPanel();
            this.wonderMailOutput = new System.Windows.Forms.RichTextBox();
            this.formRouterHost = new ExplorersOfSky.UI.Controls.FormRouterHost(this.components);
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.flowLayout.SuspendLayout();
            this.formLayout.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floorNumber)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel12.SuspendLayout();
            this.bufferedPanel1.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(55, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(420, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pokémon Mystery Dungeon: Explorers of Sky";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(180, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Wonder Mail Generator";
            // 
            // appInfoLink
            // 
            this.appInfoLink.AutoSize = true;
            this.appInfoLink.BackColor = System.Drawing.Color.Transparent;
            this.appInfoLink.Location = new System.Drawing.Point(460, 80);
            this.appInfoLink.Name = "appInfoLink";
            this.appInfoLink.Size = new System.Drawing.Size(53, 15);
            this.appInfoLink.TabIndex = 17;
            this.appInfoLink.TabStop = true;
            this.appInfoLink.Text = "App Info";
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.AutoSize = true;
            this.mainPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mainPanel.BackColor = System.Drawing.SystemColors.Control;
            this.mainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainPanel.Controls.Add(this.pictureBox2);
            this.mainPanel.Controls.Add(this.pictureBox1);
            this.mainPanel.Controls.Add(this.flowLayout);
            this.mainPanel.Controls.Add(this.label2);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Location = new System.Drawing.Point(3, 4);
            this.mainPanel.MinimumSize = new System.Drawing.Size(533, 2);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(533, 555);
            this.mainPanel.TabIndex = 3;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::ExplorersOfSky.Properties.Resources.AppIcon;
            this.pictureBox2.Location = new System.Drawing.Point(349, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::ExplorersOfSky.Properties.Resources.AppIcon;
            this.pictureBox1.Location = new System.Drawing.Point(162, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // flowLayout
            // 
            this.flowLayout.AutoSize = true;
            this.flowLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayout.BackColor = System.Drawing.Color.Transparent;
            this.flowLayout.Controls.Add(this.formLayout);
            this.flowLayout.Controls.Add(this.bufferedPanel1);
            this.flowLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayout.Location = new System.Drawing.Point(8, 66);
            this.flowLayout.Name = "flowLayout";
            this.flowLayout.Size = new System.Drawing.Size(514, 484);
            this.flowLayout.TabIndex = 18;
            // 
            // formLayout
            // 
            this.formLayout.AutoSize = true;
            this.formLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.formLayout.BackColor = System.Drawing.Color.Transparent;
            this.formLayout.ColumnCount = 2;
            this.formLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.42857F));
            this.formLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.57143F));
            this.formLayout.Controls.Add(this.panel19, 1, 10);
            this.formLayout.Controls.Add(this.panel22, 1, 5);
            this.formLayout.Controls.Add(this.panel21, 0, 5);
            this.formLayout.Controls.Add(this.panel1, 1, 0);
            this.formLayout.Controls.Add(this.panel2, 0, 0);
            this.formLayout.Controls.Add(this.panel3, 0, 1);
            this.formLayout.Controls.Add(this.panel4, 1, 1);
            this.formLayout.Controls.Add(this.panel5, 0, 2);
            this.formLayout.Controls.Add(this.panel6, 1, 2);
            this.formLayout.Controls.Add(this.panel7, 0, 3);
            this.formLayout.Controls.Add(this.panel8, 1, 3);
            this.formLayout.Controls.Add(this.panel9, 0, 4);
            this.formLayout.Controls.Add(this.panel10, 1, 4);
            this.formLayout.Controls.Add(this.panel17, 0, 9);
            this.formLayout.Controls.Add(this.panel15, 0, 8);
            this.formLayout.Controls.Add(this.panel18, 1, 9);
            this.formLayout.Controls.Add(this.panel13, 0, 7);
            this.formLayout.Controls.Add(this.panel16, 1, 8);
            this.formLayout.Controls.Add(this.panel11, 0, 6);
            this.formLayout.Controls.Add(this.panel14, 1, 7);
            this.formLayout.Controls.Add(this.panel12, 1, 6);
            this.formLayout.Location = new System.Drawing.Point(0, 0);
            this.formLayout.Margin = new System.Windows.Forms.Padding(0);
            this.formLayout.Name = "formLayout";
            this.formLayout.RowCount = 11;
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formLayout.Size = new System.Drawing.Size(514, 376);
            this.formLayout.TabIndex = 3;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.createWonderMail);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(110, 340);
            this.panel19.Margin = new System.Windows.Forms.Padding(0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(404, 36);
            this.panel19.TabIndex = 11;
            // 
            // createWonderMail
            // 
            this.createWonderMail.Location = new System.Drawing.Point(3, 2);
            this.createWonderMail.Name = "createWonderMail";
            this.createWonderMail.Size = new System.Drawing.Size(149, 30);
            this.createWonderMail.TabIndex = 14;
            this.createWonderMail.Text = "Generate Wonder Mail!";
            this.createWonderMail.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.extraTargetSelect);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(110, 170);
            this.panel22.Margin = new System.Windows.Forms.Padding(0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(404, 34);
            this.panel22.TabIndex = 6;
            // 
            // extraTargetSelect
            // 
            this.extraTargetSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.extraTargetSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.extraTargetSelect.DisplayMember = "Name";
            this.extraTargetSelect.FormattingEnabled = true;
            this.extraTargetSelect.Location = new System.Drawing.Point(3, 6);
            this.extraTargetSelect.Name = "extraTargetSelect";
            this.extraTargetSelect.Size = new System.Drawing.Size(211, 23);
            this.extraTargetSelect.TabIndex = 9;
            this.extraTargetSelect.ValueMember = "Value";
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label13);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(0, 170);
            this.panel21.Margin = new System.Windows.Forms.Padding(0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(110, 34);
            this.panel21.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label13.Location = new System.Drawing.Point(3, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Extra Target:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.missionExtraSelect);
            this.panel1.Controls.Add(this.missionTypeSelect);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(110, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 34);
            this.panel1.TabIndex = 1;
            // 
            // missionExtraSelect
            // 
            this.missionExtraSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.missionExtraSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.missionExtraSelect.DisplayMember = "Name";
            this.missionExtraSelect.FormattingEnabled = true;
            this.missionExtraSelect.Location = new System.Drawing.Point(220, 6);
            this.missionExtraSelect.Name = "missionExtraSelect";
            this.missionExtraSelect.Size = new System.Drawing.Size(155, 23);
            this.missionExtraSelect.TabIndex = 2;
            this.missionExtraSelect.ValueMember = "Value";
            // 
            // missionTypeSelect
            // 
            this.missionTypeSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.missionTypeSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.missionTypeSelect.DisplayMember = "Name";
            this.missionTypeSelect.FormattingEnabled = true;
            this.missionTypeSelect.Location = new System.Drawing.Point(3, 6);
            this.missionTypeSelect.Name = "missionTypeSelect";
            this.missionTypeSelect.Size = new System.Drawing.Size(211, 23);
            this.missionTypeSelect.TabIndex = 1;
            this.missionTypeSelect.ValueMember = "Value";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(110, 34);
            this.panel2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mission Type:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 34);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(110, 34);
            this.panel3.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Dungeon:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dungeonSelect);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(110, 34);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(404, 34);
            this.panel4.TabIndex = 2;
            // 
            // dungeonSelect
            // 
            this.dungeonSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.dungeonSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.dungeonSelect.DisplayMember = "Name";
            this.dungeonSelect.FormattingEnabled = true;
            this.dungeonSelect.Location = new System.Drawing.Point(3, 6);
            this.dungeonSelect.Name = "dungeonSelect";
            this.dungeonSelect.Size = new System.Drawing.Size(211, 23);
            this.dungeonSelect.TabIndex = 3;
            this.dungeonSelect.ValueMember = "Value";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 68);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(110, 34);
            this.panel5.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(3, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "Floor:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.floorNumber);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(110, 68);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(404, 34);
            this.panel6.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label14.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label14.Location = new System.Drawing.Point(68, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(277, 15);
            this.label14.TabIndex = 5;
            this.label14.Text = "(Make sure that the floor exists in the dungeon!)";
            // 
            // floorNumber
            // 
            this.floorNumber.Location = new System.Drawing.Point(3, 6);
            this.floorNumber.Name = "floorNumber";
            this.floorNumber.Size = new System.Drawing.Size(59, 23);
            this.floorNumber.TabIndex = 4;
            this.floorNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label6);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 102);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(110, 34);
            this.panel7.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(3, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Client:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.clientFemaleCheck);
            this.panel8.Controls.Add(this.clientSelect);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(110, 102);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(404, 34);
            this.panel8.TabIndex = 4;
            // 
            // clientFemaleCheck
            // 
            this.clientFemaleCheck.AutoSize = true;
            this.clientFemaleCheck.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.clientFemaleCheck.Location = new System.Drawing.Point(220, 8);
            this.clientFemaleCheck.Name = "clientFemaleCheck";
            this.clientFemaleCheck.Size = new System.Drawing.Size(76, 19);
            this.clientFemaleCheck.TabIndex = 6;
            this.clientFemaleCheck.Text = "Is Female";
            this.clientFemaleCheck.UseVisualStyleBackColor = true;
            // 
            // clientSelect
            // 
            this.clientSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.clientSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.clientSelect.DisplayMember = "Name";
            this.clientSelect.FormattingEnabled = true;
            this.clientSelect.Location = new System.Drawing.Point(3, 6);
            this.clientSelect.Name = "clientSelect";
            this.clientSelect.Size = new System.Drawing.Size(211, 23);
            this.clientSelect.TabIndex = 5;
            this.clientSelect.ValueMember = "Value";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label7);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 136);
            this.panel9.Margin = new System.Windows.Forms.Padding(0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(110, 34);
            this.panel9.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(3, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "Target:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.targetFemaleCheck);
            this.panel10.Controls.Add(this.targetSelect);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(110, 136);
            this.panel10.Margin = new System.Windows.Forms.Padding(0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(404, 34);
            this.panel10.TabIndex = 5;
            // 
            // targetFemaleCheck
            // 
            this.targetFemaleCheck.AutoSize = true;
            this.targetFemaleCheck.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.targetFemaleCheck.Location = new System.Drawing.Point(220, 8);
            this.targetFemaleCheck.Name = "targetFemaleCheck";
            this.targetFemaleCheck.Size = new System.Drawing.Size(76, 19);
            this.targetFemaleCheck.TabIndex = 8;
            this.targetFemaleCheck.Text = "Is Female";
            this.targetFemaleCheck.UseVisualStyleBackColor = true;
            // 
            // targetSelect
            // 
            this.targetSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.targetSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.targetSelect.DisplayMember = "Name";
            this.targetSelect.FormattingEnabled = true;
            this.targetSelect.Location = new System.Drawing.Point(3, 6);
            this.targetSelect.Name = "targetSelect";
            this.targetSelect.Size = new System.Drawing.Size(211, 23);
            this.targetSelect.TabIndex = 7;
            this.targetSelect.ValueMember = "Value";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.label11);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 306);
            this.panel17.Margin = new System.Windows.Forms.Padding(0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(110, 34);
            this.panel17.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label11.Location = new System.Drawing.Point(3, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "EUR ver. code?";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label10);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 272);
            this.panel15.Margin = new System.Windows.Forms.Padding(0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(110, 34);
            this.panel15.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(3, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Reward Item:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.eurWonderMailCheck);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(110, 306);
            this.panel18.Margin = new System.Windows.Forms.Padding(0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(404, 34);
            this.panel18.TabIndex = 10;
            // 
            // eurWonderMailCheck
            // 
            this.eurWonderMailCheck.AutoSize = true;
            this.eurWonderMailCheck.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.eurWonderMailCheck.Location = new System.Drawing.Point(3, 9);
            this.eurWonderMailCheck.Name = "eurWonderMailCheck";
            this.eurWonderMailCheck.Size = new System.Drawing.Size(313, 19);
            this.eurWonderMailCheck.TabIndex = 13;
            this.eurWonderMailCheck.Text = "(Wonder Mail is for the European version of the game)";
            this.eurWonderMailCheck.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label9);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 238);
            this.panel13.Margin = new System.Windows.Forms.Padding(0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(110, 34);
            this.panel13.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "Reward Type:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.rewardItemSelect);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(110, 272);
            this.panel16.Margin = new System.Windows.Forms.Padding(0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(404, 34);
            this.panel16.TabIndex = 9;
            // 
            // rewardItemSelect
            // 
            this.rewardItemSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.rewardItemSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.rewardItemSelect.DisplayMember = "Name";
            this.rewardItemSelect.FormattingEnabled = true;
            this.rewardItemSelect.Location = new System.Drawing.Point(3, 6);
            this.rewardItemSelect.Name = "rewardItemSelect";
            this.rewardItemSelect.Size = new System.Drawing.Size(211, 23);
            this.rewardItemSelect.TabIndex = 12;
            this.rewardItemSelect.ValueMember = "Value";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label8);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 204);
            this.panel11.Margin = new System.Windows.Forms.Padding(0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(110, 34);
            this.panel11.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(3, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Target Item:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.rewardTypeSelect);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(110, 238);
            this.panel14.Margin = new System.Windows.Forms.Padding(0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(404, 34);
            this.panel14.TabIndex = 8;
            // 
            // rewardTypeSelect
            // 
            this.rewardTypeSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.rewardTypeSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.rewardTypeSelect.DisplayMember = "Name";
            this.rewardTypeSelect.FormattingEnabled = true;
            this.rewardTypeSelect.Location = new System.Drawing.Point(3, 6);
            this.rewardTypeSelect.Name = "rewardTypeSelect";
            this.rewardTypeSelect.Size = new System.Drawing.Size(211, 23);
            this.rewardTypeSelect.TabIndex = 11;
            this.rewardTypeSelect.ValueMember = "Value";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.targetItemSelect);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(110, 204);
            this.panel12.Margin = new System.Windows.Forms.Padding(0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(404, 34);
            this.panel12.TabIndex = 7;
            // 
            // targetItemSelect
            // 
            this.targetItemSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.targetItemSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.targetItemSelect.DisplayMember = "Name";
            this.targetItemSelect.FormattingEnabled = true;
            this.targetItemSelect.Location = new System.Drawing.Point(3, 6);
            this.targetItemSelect.Name = "targetItemSelect";
            this.targetItemSelect.Size = new System.Drawing.Size(211, 23);
            this.targetItemSelect.TabIndex = 10;
            this.targetItemSelect.ValueMember = "Value";
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.Controls.Add(this.label12);
            this.bufferedPanel1.Controls.Add(this.createARCode);
            this.bufferedPanel1.Controls.Add(this.appInfoLink);
            this.bufferedPanel1.Controls.Add(this.panel20);
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 382);
            this.bufferedPanel1.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(513, 102);
            this.bufferedPanel1.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 15);
            this.label12.TabIndex = 4;
            this.label12.Text = "Wonder Mail Code:";
            // 
            // createARCode
            // 
            this.createARCode.Location = new System.Drawing.Point(403, 17);
            this.createARCode.Name = "createARCode";
            this.createARCode.Size = new System.Drawing.Size(110, 30);
            this.createARCode.TabIndex = 16;
            this.createARCode.Text = "Get AR Code";
            this.createARCode.UseVisualStyleBackColor = true;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.wonderMailOutput);
            this.panel20.Location = new System.Drawing.Point(3, 17);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(395, 78);
            this.panel20.TabIndex = 6;
            // 
            // wonderMailOutput
            // 
            this.wonderMailOutput.BackColor = System.Drawing.Color.WhiteSmoke;
            this.wonderMailOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wonderMailOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wonderMailOutput.Location = new System.Drawing.Point(0, 0);
            this.wonderMailOutput.Name = "wonderMailOutput";
            this.wonderMailOutput.ReadOnly = true;
            this.wonderMailOutput.Size = new System.Drawing.Size(393, 76);
            this.wonderMailOutput.TabIndex = 15;
            this.wonderMailOutput.Text = "";
            // 
            // formRouterHost
            // 
            this.formRouterHost.ContainerControl = this;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = global::ExplorersOfSky.Properties.Resources.EoS_Background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(538, 561);
            this.Controls.Add(this.mainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wonder Mail Generator - Explorers of Sky";
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.flowLayout.ResumeLayout(false);
            this.flowLayout.PerformLayout();
            this.formLayout.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floorNumber)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.bufferedPanel1.ResumeLayout(false);
            this.bufferedPanel1.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel appInfoLink;
        private Controls.BufferedPanel mainPanel;
        private System.Windows.Forms.TableLayoutPanel formLayout;
        private System.Windows.Forms.Label label3;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox missionTypeSelect;
        private Controls.BufferedPanel panel1;
        private Controls.BufferedPanel panel2;
        private Controls.BufferedPanel panel3;
        private System.Windows.Forms.Label label4;
        private Controls.BufferedPanel panel4;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox dungeonSelect;
        private Controls.BufferedPanel panel5;
        private System.Windows.Forms.Label label5;
        private Controls.BufferedPanel panel6;
        private System.Windows.Forms.NumericUpDown floorNumber;
        private Controls.BufferedPanel panel7;
        private System.Windows.Forms.Label label6;
        private Controls.BufferedPanel panel8;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox clientSelect;
        private Controls.BufferedPanel panel9;
        private System.Windows.Forms.Label label7;
        private Controls.BufferedPanel panel10;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox targetSelect;
        private Controls.BufferedPanel panel12;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox targetItemSelect;
        private Controls.BufferedPanel panel11;
        private System.Windows.Forms.Label label8;
        private Controls.BufferedPanel panel14;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox rewardTypeSelect;
        private Controls.BufferedPanel panel13;
        private System.Windows.Forms.Label label9;
        private Controls.BufferedPanel panel16;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox rewardItemSelect;
        private Controls.BufferedPanel panel15;
        private System.Windows.Forms.Label label10;
        private Controls.BufferedPanel panel17;
        private System.Windows.Forms.Label label11;
        private Controls.BufferedPanel panel18;
        private System.Windows.Forms.CheckBox eurWonderMailCheck;
        private System.Windows.Forms.Button createWonderMail;
        private System.Windows.Forms.Button createARCode;
        private Controls.BufferedPanel panel20;
        private System.Windows.Forms.RichTextBox wonderMailOutput;
        private System.Windows.Forms.Label label12;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox missionExtraSelect;
        private Controls.BufferedPanel panel22;
        private ExplorersOfSky.UI.Controls.CollectionItemComboBox extraTargetSelect;
        private Controls.BufferedPanel panel21;
        private System.Windows.Forms.Label label13;
        private Controls.BufferedPanel panel19;
        private System.Windows.Forms.CheckBox clientFemaleCheck;
        private System.Windows.Forms.CheckBox targetFemaleCheck;
        private System.Windows.Forms.FlowLayoutPanel flowLayout;
        private Controls.BufferedPanel bufferedPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label14;
        private Controls.FormRouterHost formRouterHost;
    }
}

