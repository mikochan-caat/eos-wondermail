using System;
using System.Drawing;
using System.Reactive.Disposables;
using System.Windows.Forms;
using ExplorersOfSky.Data;
using ExplorersOfSky.UI.Controls;
using ExplorersOfSky.UI.ViewModels;
using ReactiveUI;

namespace ExplorersOfSky.UI.Views
{
    public sealed partial class Main : BaseForm, IViewFor<MainViewModel>
    {
        private Font wonderMailFont;
        private Font wonderMailErrorFont;

        public MainViewModel ViewModel { get; set; }
        object IViewFor.ViewModel
        {
            get => this.ViewModel;
            set => this.ViewModel = (MainViewModel)value;
        }

        public Main()
        {
            this.InitializeComponent();
            this.InitializeForm();
            this.InitializeBindings();
        }

        protected override void OnLoad(EventArgs e)
        {
            // Hide Extra Target row (unused for now)
            this.formLayout.RowStyles[5].SizeType = SizeType.Absolute;
            this.formLayout.RowStyles[5].Height = 0;
            this.extraTargetSelect.Visible = false;

            base.OnLoad(e);
        }

        private void InitializeForm()
        {
            this.mainPanel.BackColor = Color.FromArgb(212, 240, 240, 240);

            this.floorNumber.Minimum = Constants.MinimumFloorNumber;
            this.floorNumber.Maximum = Constants.MaximumFloorNumber;

            this.flowLayout.ForceDoubleBuffer(true);
            this.formLayout.ForceDoubleBuffer(true);

            this.wonderMailFont = new Font("Consolas", this.wonderMailOutput.Font.Size * 1.85f);
            this.wonderMailErrorFont = this.wonderMailOutput.Font;
        }

        private void InitializeBindings()
        {
            this.WhenActivated((disposables) => {
                this.OneWayBind(this.ViewModel, vm => vm.AppIcon, v => v.Icon)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.Title, v => v.Text)
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.Missions, v => v.missionTypeSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedMission, v => v.missionTypeSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => Mission.FromStringValue((string)vProp))
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.MissionSubTypes, v => v.missionExtraSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedMissionSubType, v => v.missionExtraSelect.SelectedIndex)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.IsMissionSubTypesVisible, v => v.missionExtraSelect.Visible)
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.Dungeons, v => v.dungeonSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedDungeon, v => v.dungeonSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => Dungeon.FromStringValue((string)vProp))
                    .DisposeWith(disposables);

                this.Bind(this.ViewModel, vm => vm.InputtedFloorNumber, v => v.floorNumber.Value,
                          vmProp => vmProp, vProp => (int)vProp)
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.ClientList, v => v.clientSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedClient, v => v.clientSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => Pokemon.FromStringValue((string)vProp))
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.IsClientSelectable, v => v.clientSelect.Enabled)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.IsClientGenderSelectable, v => v.clientFemaleCheck.Enabled)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.IsClientFemale, v => v.clientFemaleCheck.Checked)
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.TargetList, v => v.targetSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedTarget, v => v.targetSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => Pokemon.FromStringValue((string)vProp))
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.IsTargetSelectable, v => v.targetSelect.Enabled)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.IsTargetGenderSelectable, v => v.targetFemaleCheck.Enabled)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.IsTargetFemale, v => v.targetFemaleCheck.Checked)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.TargetItems, v => v.targetItemSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedTargetItem, v => v.targetItemSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => Item.FromStringValue((string)vProp))
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.CanSelectTargetItem, v => v.targetItemSelect.Enabled)
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.RewardTypes, v => v.rewardTypeSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedRewardType, v => v.rewardTypeSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => RewardType.FromStringValue((string)vProp))
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.CanSelectReward, v => v.rewardTypeSelect.Enabled)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.RewardItems, v => v.rewardItemSelect.DataSource)
                    .DisposeWith(disposables);
                this.Bind(this.ViewModel, vm => vm.SelectedRewardItem, v => v.rewardItemSelect.SelectedValue,
                          vmProp => vmProp.Value.ToString(), vProp => Item.FromStringValue((string)vProp))
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.CanSelectRewardItem, v => v.rewardItemSelect.Enabled)
                    .DisposeWith(disposables);

                this.Bind(this.ViewModel, vm => vm.IsEuroWonderMail, v => v.eurWonderMailCheck.Checked)
                    .DisposeWith(disposables);

                this.BindCommand(this.ViewModel, vm => vm.CreateWonderMail, v => v.createWonderMail)
                    .DisposeWith(disposables);
                this.BindCommand(this.ViewModel, vm => vm.GetARCode, v => v.createARCode)
                    .DisposeWith(disposables);
                this.BindCommand(this.ViewModel, vm => vm.DisplayAppInfo, v => v.appInfoLink)
                    .DisposeWith(disposables);

                this.ViewModel.WhenAnyValue(vm => vm.CreatedWonderMailCode, vm => vm.IsValidWonderMail)
                    .Subscribe(obs => this.HandleWonderMailOutput(obs.Item1, obs.Item2))
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.FormRouter, v => v.formRouterHost.Router)
                    .DisposeWith(disposables);

                this.ViewModel.CommonEvents.NotificationHandler.RegisterHandler(this.DisplayNotificationDefault)
                    .DisposeWith(disposables);
                this.ViewModel.CommonEvents.ConfirmationHandler.RegisterHandler(this.DisplayConfirmationDefault)
                    .DisposeWith(disposables);

                this.wonderMailFont.DisposeWith(disposables);
            });
        }

        private void HandleWonderMailOutput(string code, bool isValid)
        {
            this.wonderMailOutput.Text = isValid ? PrettyWonderMail.Format(code) : code;
            this.wonderMailOutput.Font = isValid ? this.wonderMailFont : this.wonderMailErrorFont;

            if (isValid) {
                this.wonderMailOutput.SelectionStart = this.wonderMailOutput.Text.Length;
                this.wonderMailOutput.SelectionLength = 0;
            }
        }
    }
}
