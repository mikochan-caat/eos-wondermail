using System;
using System.Reactive.Disposables;
using System.Windows.Forms;
using ExplorersOfSky.UI.Controls;
using ExplorersOfSky.UI.Notifications;
using ExplorersOfSky.UI.ViewModels;
using ReactiveUI;

namespace ExplorersOfSky.UI.Views
{
    public sealed partial class ARCode : BaseForm, IViewFor<ARCodeViewModel>
    {
        public ARCodeViewModel ViewModel { get; set; }
        object IViewFor.ViewModel
        {
            get => this.ViewModel;
            set => this.ViewModel = (ARCodeViewModel)value;
        }

        public ARCode()
        {
            this.InitializeComponent();
            this.InitializeBindings();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            this.createdARCode.Focus();
        }

        private void InitializeBindings()
        {
            this.WhenActivated((disposables) => {
                this.OneWayBind(this.ViewModel, vm => vm.Title, v => v.Text)
                    .DisposeWith(disposables);

                this.OneWayBind(this.ViewModel, vm => vm.WonderMailCode, v => v.wonderMailCode.Text)
                    .DisposeWith(disposables);
                this.OneWayBind(this.ViewModel, vm => vm.CreatedARCode, v => v.createdARCode.Text)
                    .DisposeWith(disposables);

                this.ViewModel.CommonEvents.OnRequestClose.Subscribe(_ => this.Close())
                    .DisposeWith(disposables);
                this.ViewModel.CommonEvents.NotificationHandler.RegisterHandler(this.DisplayNotificationDefault)
                    .DisposeWith(disposables);
                this.ViewModel.CommonEvents.ConfirmationHandler.RegisterHandler(this.DisplayConfirmation)
                    .DisposeWith(disposables);

                this.ViewModel.PromptOnLoad.Execute().Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void DisplayConfirmation(InteractionContext<NotificationMessage, ConfirmationResult> context)
        {
            switch (context.Input.Identifier) {
                case var id when id == String.Empty:
                    this.DisplayConfirmationDefault(context);
                    break;
                default:
                    this.DisplayConfirmationDefault(context, (context) => new MessageDialog.Options {
                        Caption = context.Input.Title,
                        Buttons = MessageBoxButtons.OKCancel,
                        DefaultButton = MessageBoxDefaultButton.Button1,
                        Icon = MessageBoxIcon.Information
                    });
                    break;
            }
        }
    }
}
