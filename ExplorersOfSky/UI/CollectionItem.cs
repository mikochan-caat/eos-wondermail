using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ExplorersOfSky.Data;

namespace ExplorersOfSky.UI
{
    public sealed class CollectionItem
    {
        private static readonly CollectionItem[] EmptyCollection = new CollectionItem[0];

        public static readonly CollectionItem EmptyItem = new CollectionItem {
            Name = String.Empty,
            Value = String.Empty
        };

        public string Name { get; private set; }
        public string Value { get; private set; }

        public static IList<CollectionItem> Empty() => EmptyCollection;

        public static IList<CollectionItem> FromEnumerable<T>(IEnumerable<T> src)
            => FromEnumerable(src, false);

        public static IList<CollectionItem> FromEnumerable<T>(IEnumerable<T> src, bool includeEmptySlot)
            => FromEnumerable(src, srcVal => srcVal.ToString(), srcVal => srcVal.ToString(), includeEmptySlot);

        public static IList<CollectionItem> FromEnumerable<T>(
            IEnumerable<T> src, Func<T, string> nameFunc, Func<T, string> valueFunc)
            => FromEnumerable(src, nameFunc, valueFunc, false);

        public static IList<CollectionItem> FromEnumerable<T>(
            IEnumerable<T> src, Func<T, string> nameFunc, Func<T, string> valueFunc, bool includeEmptySlot)
        {
            var output = src
                .Select(value => new CollectionItem {
                    Name = nameFunc(value),
                    Value = valueFunc(value)
                });

            if (includeEmptySlot) {
                output = output.Prepend(EmptyItem);
            }

            return output.ToList();
        }
        public static IList<CollectionItem> FromNameValues<T>(IEnumerable<T> src)
            where T : INameValuePair
            => FromNameValues(src, false);

        public static IList<CollectionItem> FromNameValues<T>(IEnumerable<T> src, bool includeEmptySlot)
            where T : INameValuePair
            => FromEnumerable(src, item => item.Name, item => item.Value, includeEmptySlot);
    }

    public static class CollectionItemExtensions
    {
        public static void ConfigureDataMemberBindings(this ListControl control)
        {
            control.DisplayMember = nameof(CollectionItem.Name);
            control.ValueMember = nameof(CollectionItem.Value);
        }
    }
}
