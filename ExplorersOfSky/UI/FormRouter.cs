using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using ExplorersOfSky.UI.ViewModels.Extensible;
using ReactiveUI;

namespace ExplorersOfSky.UI
{
    public interface IFormRouter
    {
        IObservable<FormRouter.IRouteMessage> NavigationChanged { get; }

        Task<T> Navigate<T>(T viewModel) where T : IFormRoutableViewModel;
        Task<T> Navigate<T>(T viewModel, bool isModal) where T : IFormRoutableViewModel;
    }

    public sealed class FormRouter : IFormRouter
    {
        private readonly Subject<IRouteMessage> viewModelSubject;

        public IObservable<IRouteMessage> NavigationChanged => this.viewModelSubject.AsObservable();

        public FormRouter()
        {
            this.viewModelSubject = new Subject<IRouteMessage>();
        }

        public async Task<T> Navigate<T>(T viewModel) where T : IFormRoutableViewModel
            => await this.Navigate(viewModel, false);

        public async Task<T> Navigate<T>(T viewModel, bool isModal) where T : IFormRoutableViewModel
        {
            return await Observable
                .Start(() => viewModel, RxApp.MainThreadScheduler)
                .Do(vm => {
                    this.viewModelSubject.OnNext(new NavigationMessage {
                        ViewModel = vm,
                        IsModal = isModal
                    });
                });
        }

        public interface IRouteMessage
        {
            IFormRoutableViewModel ViewModel { get; }
        }

        public sealed class NavigationMessage : IRouteMessage
        {
            public IFormRoutableViewModel ViewModel { get; set; }
            public bool IsModal { get; set; }
        }
    }
}
